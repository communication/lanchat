/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* die config aller sockets holen und in ifc ablegen */
#include <stddef.h>
#include <openssl/aes.h>
#include <openssl/md5.h>
#include "chatconfig.h"

#ifndef _md5key
#define _md5key
struct md5key {
char key_md5[16];
char room_md5[16];
AES_KEY key_enc;
AES_KEY key_dec;
};
#endif

/* aus dem eingabetext alle keys und md5summen erstellen
   alle md5 summen müssen mit free freigegeben werden */
int makemd5(const char *keytext, struct md5key *keys); 

/* BinPack text nach daten verschlüsseln  
   *daten muss mit free freigegen werden  */
int Cencrypt(const struct BinPack *text, struct BinPack *daten, struct md5key *keys);

/* BinPack daten nach text entschlüsseln 
   *text muss mit free freigegen werden  */
int Cdecrypt(const struct BinPack *daten, struct BinPack *text, struct md5key *keys);


