/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <dirent.h>
#include <regex.h>
#include "vorbis/codec.h"
#include "vorbis/vorbisfile.h"
#include <ao/ao.h>
#include <unistd.h>

#include "oggerer.h"

int playinit(){
  ao_initialize();
  return 1;
}

int playshutdown(){
  ao_shutdown();
  return 1;
}

int oggerer(const char *dirname, const char *text){
  
    char *dirname2 = strdup(dirname);
    if(!dirname2)
      return 0;
    struct oggmatch **list;
    list = calloc(sizeof(struct oggmatch *), strlen(text));
    DIR *dp;
    struct dirent *ep;
    int dirnamelen = strlen(dirname);
    if(dirname2[dirnamelen] != '/'){
      dirname2 = realloc(dirname2, dirnamelen+2);
      if(!dirname2)
        return 0;
      dirname2[dirnamelen] = '/';
      dirname2[dirnamelen+1] = '\0';
    }

    
    char *filepfad;
    dp = opendir (dirname2);
    if (dp != NULL){
      while ((ep = readdir (dp))){
       if(ep->d_name[0] != '.'){
        /* regex */
        char pattern[]="^.*ogg$";
        size_t regex_nmatch=2;
        regex_t regex;
        regmatch_t regex_pmatch[regex_nmatch];

        char *match=strdup(ep->d_name);
        if (regcomp(&regex,pattern,REG_EXTENDED) == 0 && regexec(&regex,match,regex_nmatch,regex_pmatch,0) == 0){
          filepfad = calloc(strlen(dirname2) + strlen(ep->d_name)+1,1);
          if(!filepfad)
            return 0;
          strcat(filepfad, dirname2);
          strcat(filepfad, ep->d_name);
          parsefiles(filepfad, text, list);
          free(filepfad);
        }
      }
    }
    closedir (dp);
    unsigned int i;
    for(i = 0; i < strlen(text);i++){
      while(list[i]){
        //fprintf(stderr, "match: %d, %s\n", i, list[i]->filename);
        int back = playfile(list[i]->filename);
        if(!back)
          return 0;
        list[i]=list[i]->next;
      }
    }

  }else{
      perror ("Couldn't open the directory");
  }
  free(dirname2);
  return 1;
}
  

  
int parsefiles(const char *name, const char *text, struct oggmatch **list){
  OggVorbis_File vf;
  //int eof=0;
  //int current_section;
  FILE *oggfile;
  oggfile = fopen(name, "r");
  //fprintf(stderr,"geoffnet: %s\n", name);

  if(ov_open(oggfile, &vf, NULL, 0) < 0) {
    fprintf(stderr,"Das ist keine Oggdatei.\n");
    return(0);
  }

  char *regex_pattern;
  char **ptr=ov_comment(&vf,-1)->user_comments;
    //vorbis_info *vi=ov_info(&vf,-1);
  while(*ptr){
      //fprintf(stderr,"->%s\n",*ptr);
    if(strncmp(*ptr, "comments=", 9)){
      regex_pattern = strchr(*ptr, '=') + 1;
      /* regex */
        //char pattern[]="^.*ogg$";
      size_t regex_nmatch=2;
      regex_t regex;
      regmatch_t regex_pmatch[regex_nmatch];
      
      char *match=strdup(text);
      if (regcomp(&regex,regex_pattern,REG_EXTENDED)==0&&
          regexec(&regex,match,regex_nmatch,regex_pmatch,0)==0){
        //fprintf(stderr, "position: %d, file:%s\n", regex_pmatch[0].rm_so,name);
        struct oggmatch *schonda = list[regex_pmatch[0].rm_so];
            
        list[regex_pmatch[0].rm_so] = malloc(sizeof(struct oggmatch));
        if(!list[regex_pmatch[0].rm_so])
          return 0;

        list[regex_pmatch[0].rm_so]->filename = strdup(name);
        if(!list[regex_pmatch[0].rm_so]->filename)
          return 0;
        list[regex_pmatch[0].rm_so]->next = schonda;

          }

    }
    ++ptr;
  }
  fclose(oggfile);
  return 1;
}

int playfile(const char *name){
  char pcmout[4096];
  /*ogg */
  OggVorbis_File vf;
  int eof=0;
  int current_section;
  /*ao */
  ao_device *device;
  ao_sample_format format;
  int default_driver;
  //char *buffer;
  //int buf_size;


  //ao_initialize();
  default_driver = ao_default_driver_id();
  FILE *oggfile;
  oggfile = fopen(name, "r");
  //fprintf(stderr,"geoffnet: %s\n", name);

  if(ov_open(oggfile, &vf, NULL, 0) < 0) {
    fprintf(stderr,"Das ist keine Oggdatei.\n");
    return(0);
  }

    //char *regex_pattern;
  vorbis_info *vi=ov_info(&vf,-1);
/*  char **ptr=ov_comment(&vf,-1)->user_comments;
  while(*ptr){
    fprintf(stderr,"->%s\n",*ptr);
    ++ptr;
}*/
  
    //fprintf(stderr,"\nBitstream is %d channel, %ldHz\n",vi->channels,vi->rate);
    //fprintf(stderr,"\nDecoded length: %ld samples\n", (long)ov_pcm_total(&vf,-1));
    //fprintf(stderr,"Encoded by: %s\n\n",ov_comment(&vf,-1)->vendor);
  /*ao*/
  format.bits = 16;
  format.channels = vi->channels;
  format.rate = vi->rate;
  format.byte_format = AO_FMT_LITTLE;

  


  device = ao_open_live(default_driver, &format, NULL /* no options */);
  if (device == NULL) {
    sleep(15);
    device = ao_open_live(default_driver, &format, NULL);
    if (device == NULL) {
      //fprintf(stderr, "Error opening device.\n");
      return 0;
    }
  }

  //buf_size = 0;
  //buffer = NULL;

  while(!eof){
    long ret=ov_read(&vf,pcmout,sizeof(pcmout),0,2,1,&current_section);
    //long ret=ov_read(&vf,buffer,buf_size,0,2,1,&current_section);
    if (ret == 0) {
      /* EOF */
      eof=1;
    } else if (ret < 0) {
      /* error in the stream.  Not a problem, just reporting it in
      case we (the app) cares.  In this case, we don't. */
    } else {
      /* we don't bother dealing with sample rate changes, etc, but
      you'll have to*/
      ao_play(device, pcmout, ret);
    }
  }

  /*ao*/
  //ao_play(device, buffer, buf_size);

  ov_clear(&vf);
    
  /*ao*/
  ao_close(device);
    
  //ao_shutdown();

  //fprintf(stderr,"Done.\n");
  return(1);
}



