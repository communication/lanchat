/* -*- mode: c, coding: utf-8 -*- */
/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#define _XOPEN_SOURCE_EXTENDED
#define _GNU_SOURCE

#include <signal.h>

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifdef HAVE_NCURSESW_CURSES_H
# include <ncursesw/curses.h>
#elif defined(HAVE_CURSES_H)
# include <curses.h>
#endif

#include <sys/types.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
//#include <sys/wait.h>
#include <pthread.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <net/if.h>
#include <netdb.h>
#include <errno.h>

#include <openssl/aes.h>
#include <openssl/md5.h>
#include <libxml/xpath.h>
#include <langinfo.h>
#include <locale.h>
#include <libxml/debugXML.h>


#include "config.h"
#include "chatsend.h"
#include "chatcrypt.h"
#include "protokoll.h"
#include "oggerer.h"

/* maximale anzahl der dev die verwendet wird */
//#define PORT 8765
#define BUF 1024
#define MAXCHILD 3
#define DEFAULTOGGDIR ".ogg/"
//#define _THREAD
#define _FORK

/* wir müssen die Tastenkombination Strg+C abfangen 
 * und ggf. ncurses das Terminal wieder aufräumen lassen
 */
void finisher(int sig);

/* text ggf zu utf8 konvertieren */
char *ccIsoToUtf8(char **text);

/* text ggf zu iso/latin konvertieren */
char *ccUtf8ToIso(char ** text);

/* namen zuschneiden wenn er zu lang ist */
char *cutName(const char *name);

#ifdef _THREAD
struct playlisteintrag;

struct playlisteintrag{
  struct playlisteintrag *next;
  char *text;
};

struct threadinfo{
  struct playlisteintrag *playliste;
  char *oggdir;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
};

//static pthread_mutex_t mutex; // = PTHREAD_MUTEX_INITIALIZER;
//static pthread_cond_t cond; // = PTHREAD_COND_INITIALIZER;

//static struct playlisteintrag *ple = NULL;
//static int vorw = 0;


void *playthread(void *arg)
{
  //pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  //(void) arg;
  struct threadinfo *ple_z = ((struct threadinfo *)arg);
  playinit();
  //fprintf(stderr, "vor while ");
  //pthread_mutex_unlock(&mutex);
  while (1){
    //pthread_mutex_lock(&mutex);
    //fprintf(stderr, "vor wait ");
    //vorw = 1;
    if(!ple_z->oggdir){
        //fprintf(stderr, "thread exit");
        //sleep(5);
        break;
    }

    pthread_mutex_lock(&ple_z->mutex);
    struct playlisteintrag *ple = ple_z->playliste;
    struct playlisteintrag *org_ple;
    pthread_mutex_unlock(&ple_z->mutex);
    //fprintf(stderr, "nach wait: %d", (int) ple);
    while(ple){
      oggerer(ple_z->oggdir, ple->text);
      pthread_mutex_lock(&ple_z->mutex);
      ple = ple->next;
      pthread_mutex_unlock(&ple_z->mutex);
    }
    pthread_mutex_lock(&ple_z->mutex);
    ple = ple_z->playliste;
    while(ple){
      org_ple = ple;
      ple = ple->next;
      free(org_ple->text);
      free(org_ple);
    }
    ple_z->playliste = NULL;
    pthread_mutex_unlock(&ple_z->mutex);
    pthread_cond_wait(&ple_z->cond, &ple_z->mutex);
    pthread_mutex_unlock(&ple_z->mutex);
  }
  playshutdown();
  return NULL;
}
#endif

#ifdef _FORK

int playfork(int pipefiled){
  FILE *rpipe = fdopen(pipefiled, "r");
  char dirbuf[1024];
  char textbuf[1024];
  //int hauab = 1;
  playinit();
  while(1){
    //fscanf(rpipe, "%[^\n]", dirbuf);
    if(!fgets(dirbuf, sizeof(dirbuf), rpipe))
      break;
    dirbuf[strlen(dirbuf)-1] = '\0';
    //fprintf(stderr, "%s\n", dirbuf);
    if(!fgets(textbuf, sizeof(textbuf), rpipe))
      break;
    textbuf[strlen(textbuf)-1] = '\0';
    //fprintf(stderr, "%s\n", textbuf);
    oggerer(dirbuf, textbuf);
    
  }
  playshutdown();
  fclose(rpipe);
  return 0;
}


#endif

int main(int argc, char *argv[])
{
  /* wir verknüpfen das INT-Signal von Linux mit der Funktion finisher */
  signal(SIGINT, finisher);
  
  int orgy,orgx;//start y,x von eingabewin
  //int ch = 0;//gelesenes zeichen
  wint_t wch; //UTF8 gelesenes zeichen 
  wint_t Lwch;
  int isLwch = 0;
  //int lch = 0;
  //int maxchild = 3;
  //int child_pid[MAXCHILD] = { 0 }; //für fork
  //struct playlisteintrag *ple = NULL;
#ifdef _THREAD
  struct threadinfo ple_z;
  ple_z.playliste = NULL;
  
  pthread_t player_thread;
  int playthreadrun = 0;
  //pthread_init();
  //mutex = PTHREAD_MUTEX_INITIALIZER;
  //cond = PTHREAD_COND_INITIALIZER;

  //pthread_mutexattr_t attr;
  //pthread_mutexattr_init(&attr);
  //pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK_NP);

  pthread_mutex_init(&ple_z.mutex, NULL);
  pthread_cond_init(&ple_z.cond, NULL);
#endif
  

#ifdef _FORK
  
  int pipefiled[2];
  
  /*Puffer zum Lesen (max. 1024 Bytes)*/
  //char pipebuf[1024];
 
  /*Pipe anlegen*/
  if (pipe(pipefiled)<0)
  {
   printf ("Fehler bei der Erzeugung der Pipe !\n");
   return 1;
  }
  FILE *wpipe;


  int child_pid = fork();
  switch(child_pid)
  {
    case -1: // fork() ist fehlgeschlagen
      perror("fork");
      return 1;
    case 0: // Das ist der Child-Prozess
      {
      /*Schreibhandle schließen*/
      close(pipefiled[1]);
      int ex = playfork(pipefiled[0]);
      close(pipefiled[0]);
      //free(ausgabetextfork);
      exit(ex);}
    default: // Das ist der Vater-Prozess
      /*Lesehandle schließen*/
      close(pipefiled[0]);
      wpipe = fdopen(pipefiled[1], "w");
      setvbuf(wpipe, NULL, _IOLBF, 0);
      //isfork = 1;
      // child_pid enthält die PID des Child-Prozesses
  } 


#endif


  /* letzte eingabezeile init */
  char *Lzeile = strdup(" ");
  if(! Lzeile) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 1;
  }
  /* aktuelle eingabezeile init */
  char *Azeile = strdup(" ");
  if(! Azeile) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 1;
  }
  int isAzeile = 0;
    
  setlocale(LC_ALL, ""); 
  //int isnotUTF8 = strcmp(nl_langinfo(CODESET),"UTF-8");
  char *userkey = NULL;
  struct md5key keys;
 
  char *defaultprofile = "default";
  /* chatconfig beschaffen */
  struct ChatConfig *config = initChatConfig(defaultprofile);
  if(! config) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 1;
  }
  config->uiname = strdup("ncursesw");
  if(! config->uiname) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 1;
  }

    
  (void) argc; 
  argv ++; /* programmnamen ueberspringen */
  /* lesen der kommandzeilenparameter */
  while(*argv) {
    if(! argv[1]) {
      break;
    }
    /* -k key lesen */
    if(! strcmp(*argv, "-k")){
      userkey = strdup(argv[1]);
      if(! userkey) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return 1;
      }

      ccIsoToUtf8(&userkey);
    }
    /* -c configfile nehmen anstatt standardconfig */
    else if(! strcmp(*argv, "-c")){
      config->configfilename = strdup(argv[1]);
      if(! config->configfilename) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return 1;
      }
    }
    /* -p profil beim start verwenden */
    else if(! strcmp(*argv, "-p")){
      config->profilename = strdup(argv[1]);
      if(! config->profilename) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return 1;
      }
    }
             
    argv += 2;
  }

  /* config von datei lesen */
  int configgeladen =  configrw(READ_CONFIG, config);
 
  /* daten fuers protokoll init */
  struct ProtConfig *pc = protokollInit(config,&keys);
  if(! pc){
    fprintf (stderr, "Fehler bei protokoll init");
    return 1;
  }
  reloadHostlist(pc);
  
  
  if(configgeladen){
    if(!userkey){
      userkey = config->userkey;
      //ccUtf8ToIso(&userkey); 
    }else{
      config->userkey = userkey;
      //ccIsoToUtf8(config->userkey);
    }

  }

  xmlChar **script = NULL;
  xmlNode *scriptNode = NULL;
  xmlChar **oggdir = NULL;
  xmlNode *oggdirNode = NULL;
  /* wenn noch keine node für uiconfig da ist dann eine erzeugen */
  //xmlDebugDumpOneNode(stderr, config->uiconfig->children->children,0);
  if(!config->uiconfig && config->ns){
    config->uiconfig = xmlNewNode(config->ns, (unsigned char*)"uiconfig");
    if(! config->uiconfig) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(config->uiconfig,(unsigned char*) "name",(unsigned char*) config->uiname);

  

    DIR *dp = opendir (DEFAULTOGGDIR);
    if (dp != NULL){

      oggdirNode = xmlNewTextChild(config->uiconfig, config->ns, (unsigned char*)"oggdir", (unsigned char*)DEFAULTOGGDIR);
      if(! oggdirNode) {
        fprintf(stderr, "unable to allocate child node\n");
        return 0;
      }
      xmlNewProp(oggdirNode,(unsigned char*) "content",(unsigned char*) "text");
      closedir(dp);
    }
    


    
  }

  /* node fürs script zuweisen */ 
  if(config->uiconfig && config->uiconfig->children){
    xmlNode *child = config->uiconfig->children;
    while(child){    
      if(strcmp((char*)child->name, "script") == 0){
        scriptNode = child;
        if(child->children &&  strcmp((char*)child->children->name, "text") == 0){
          script = &child->children->content;
          if(script && *script && strlen((char*)*script) < 2){
            xmlUnlinkNode(scriptNode->children);
            *script = NULL;
            script = NULL;
            
          }
        }
      }
      if(strcmp((char*)child->name, "oggdir") == 0){
        oggdirNode = child;
        if(child->children &&  strcmp((char*)child->children->name, "text") == 0){
          oggdir = &child->children->content;
          if(oggdir && *oggdir && strlen((char*)*oggdir) < 2){
            xmlUnlinkNode(scriptNode->children);
            *oggdir = NULL;
            oggdir = NULL;
          }
        }
      }
      
      child = child->next;
    }
  }

  if(!oggdirNode){
    oggdirNode = xmlNewTextChild(config->uiconfig, config->ns, (unsigned char*)"oggdir", (unsigned char*)NULL);
    if(! oggdirNode) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(oggdirNode,(unsigned char*) "content",(unsigned char*) "text");
  }

  if(!scriptNode){
    scriptNode = xmlNewTextChild(config->uiconfig, config->ns, (unsigned char*)"script", (unsigned char*)NULL);
    if(! scriptNode) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(scriptNode,(unsigned char*) "content",(unsigned char*) "text");
  }

  
  /*ab hier der empfaenger init */
  fd_set fds; //filedescriptor set für select
  int fd_read, rx_read; //fd von dem gelesen werden kann
  struct sockaddr_in servAddr;
  struct BinPack recvdaten;
  recvdaten.daten = malloc(BUF);
  if(! recvdaten.daten) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 1;
  }

  int rxsock; //empfangssocket
  rxsock = socket (AF_INET, SOCK_DGRAM, 0);
  if (rxsock < 0) {
    fprintf (stderr, "Kann RX Socket nicht öffnen\n");
    return(1);
  }
  /* Lokalen Server Port bind(en) */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
  servAddr.sin_port = htons (config->port);
  
  /* socket optionen setzen*/
  int i_opt = 1; //socketoption
  setsockopt(rxsock, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof( i_opt ));

  int rc;
  rc = bind ( rxsock, (struct sockaddr *) &servAddr,
              sizeof (servAddr));
  if (rc < 0) {
    fprintf (stderr, "Kann die Portnummern %d nicht bind(en)\n", PORT);
    return(1);
  }
  
  if(userkey){
    makemd5(userkey, &keys);
    /* bin da senden */  
    struct BinPack sendtext;
    config->notify=strdup("online");
    packtext("Bin auf Empfang", &sendtext, pc);
    chatsend(&sendtext, pc, NULL);
    free(sendtext.daten);
  }
  
   
  /* curses initialisieren, siehe man ncurses */
  initscr();
  cbreak();
  noecho();
  start_color();
  use_default_colors();
  /* da bei mir eh nicht mehr als 8 farben gingen die 8 standartwerte nehmen */
  init_pair(1,COLOR_GREEN,-1);
  init_pair(2,COLOR_BLUE,-1);
  init_pair(3,COLOR_YELLOW,-1);
  init_pair(4,COLOR_MAGENTA,-1);
  init_pair(5,COLOR_CYAN,-1);
  init_pair(6,COLOR_RED,-1);
  init_pair(7,COLOR_WHITE,-1);
  init_pair(8,COLOR_BLACK,-1);
  init_pair(20,COLOR_RED,COLOR_WHITE);
  
  nonl();
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);

  /*neue unterfenster fuer ein/ausgabe*/
  WINDOW *auswin = subwin(stdscr, LINES-2,COLS,0,0);
  int lastLines = LINES;
  WINDOW *einwin = subwin(stdscr, 1,COLS,LINES-2,0);
  WINDOW *statwin = subwin(stdscr, 1,COLS,LINES-1,0);
  WINDOW *helpwin = NULL;
  /* init der unterfenster */
  intrflush(einwin, FALSE);
  keypad(einwin, TRUE);
  /*cursor auf 0,0 init */
  wmove(auswin, 0, 0);
  wmove(einwin, 0, 0);
  wmove(statwin, 0, 0);
  //wprintw(auswin, "%d,%d", COLORS , COLOR_PAIRS);
  /* text für die eingabemarkierung */
  if(userkey){
    /* text ausgeben */
    if( config->hostlistlen > 0){
      wprintw(auswin, "Extrahosts: %d. ", config->hostlistlen);
      //waddstr(auswin, hostlistlentext);
    }
    if(config->logfile){
      wprintw(auswin, "Logfile: %s, ", config->logfile);
      //waddstr(auswin, hostlistlentext);
    }
    waddstr(auswin, "Bereit wenn Sie es sind!\n");
    waddstr(einwin, "--> ");
  }else{
    /* text ausgeben */
    waddstr(auswin, "Bitte den Key/Raumnamen eingeben!\n");
    waddstr(einwin, "Key:");
  }
  
  /* eingabeanfang markieren */
  getyx(einwin,orgy,orgx);
  //mvwprintw(einwin, orgy+1, 0, "Key: %s", userkey);
  wmove(einwin,orgy,orgx);
  /*ausgabewin scrollen einschalten */
  scrollok(auswin, true); 
  
  refresh();
  int chok = ERR;  

  /* ab hier socket und eingabe verarbeitung */
  for(;;) {
    memset (recvdaten.daten, 0, BUF); //empfangsbuffer leeren
    wrefresh(auswin);
    wrefresh(einwin);
    
    /* select init und auf aktivitaet der fd warten */
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    FD_SET(rxsock, &fds);
    select(rxsock+1, &fds, NULL, NULL, NULL);
  
    /* pruefen auf welchen socket sich was getan hat */
    fd_read = FD_ISSET(0, &fds);
    rx_read = FD_ISSET(rxsock, &fds);
   
    /* zeichen eingabe verarbeiten */ 
    if(fd_read == 1){ 
      chok = wget_wch(einwin, &wch);
    }

/*
    int status;
    int pids;
    for(pids = 0;pids < MAXCHILD;pids++){
      if(child_pid[pids] && waitpid(child_pid[pids], &status, WNOHANG) == child_pid[pids]){
        child_pid[pids] = 0;
      }
    
    }
*/
        
      
    /* socketdaten verarbeiten */ 
    if(rx_read == 1){
      /* socketdaten einlesen */      
      struct ClientConfig *msgconfig = chatrecv(&recvdaten, rxsock);
      /* pruefen ob gueltige daten gelesen wurden */
      if(msgconfig){
        /* ausgeben */
        char *ausgabetext;
        
        msgconfig->sec = 0;
        msgconfig->notify = NULL;
        int entpackok = entpacktext(&recvdaten, &ausgabetext, msgconfig, pc);
        if(entpackok){
          if(strlen(ausgabetext) > 0){
            if(oggdir && *oggdir){
#ifdef _THREAD
              ple_z.oggdir = (char *)*oggdir;
              //pthread_mutex_lock(&mutex);
              pthread_mutex_lock(&ple_z.mutex);
              struct playlisteintrag *akt_ple = ple_z.playliste;
              pthread_mutex_unlock(&ple_z.mutex);
              //pthread_mutex_unlock(&mutex);
              //struct playlisteintrag *ple = *ple_z;
              struct playlisteintrag *pre_ple = NULL;
              //int i = 0;
              while(akt_ple){
                //wprintw(auswin, "%d,",i++);
                pre_ple = akt_ple;
                akt_ple = akt_ple->next;
              }
              //wprintw(auswin, "ple: %d", (int) ple);
              akt_ple = malloc(sizeof(struct playlisteintrag));
              if(!akt_ple)
                return 1;
              if(pre_ple)
                pre_ple->next = akt_ple;
              akt_ple->next = NULL;
              akt_ple->text = strdup(ausgabetext);
              if(!akt_ple->text)
                return 1;
              pthread_mutex_lock(&ple_z.mutex);
              if(!ple_z.playliste){
                ple_z.playliste = akt_ple;
              }
              pthread_mutex_unlock(&ple_z.mutex);
              //*ple_z = ple;
              //wprintw(auswin, "ple: %d, ", (int) ple);
              //pthread_mutex_unlock(&mutex);
              //pthread_mutex_lock(&mutex);
              //int rc = pthread_cond_signal(&cond);
              if(playthreadrun){
                pthread_cond_broadcast(&ple_z.cond);
                //wprintw(auswin, "rc: %d\n", rc);
              }else{
                playthreadrun = 1;
                pthread_create(&player_thread, NULL, playthread, &ple_z);
              }
#endif

#ifdef _FORK
              fprintf(wpipe, "%s\n", (char *)*oggdir);
              fprintf(wpipe, "%s\n", ausgabetext);



#endif

              /* 
              char *ausgabetextfork = strdup(ausgabetext);
              if(!ausgabetextfork)
                return 1;
              int childnr= MAXCHILD -1;
              while(childnr >= 0){
                if(child_pid[childnr] == 0)
                  break;
                childnr--;
              }
              if(childnr >= 0){
              child_pid[childnr] = fork();
              switch(child_pid[childnr])
              {
                case -1: // fork() ist fehlgeschlagen
                  perror("fork");
                  return 1;
                case 0: // Das ist der Child-Prozess
                  {int ex = oggerer((char*)*oggdir, ausgabetextfork);
                  //free(ausgabetextfork);
                  exit(ex);}
                default: // Das ist der Vater-Prozess
                  ;//isfork = 1;
                    // child_pid enthält die PID des Child-Prozesses
              } 
              //oggerer((char*)*oggdir, ausgabetext);
              }
              */

            }  

            ccUtf8ToIso(&ausgabetext);
            
            /* farbe für den user finden und setzen */
            int clients = 0;
            while((pc->clients[clients]).host != 0){
              if(pc->clients[clients].host == msgconfig->host && strcmp(pc->clients[clients].username, msgconfig->username) == 0){
                break;
              }
              clients++;
            }
            wcolor_set(auswin, clients%9, NULL);

            /* pruefen ob statusmeldung mit dabei ist (online, offline) */
            if(msgconfig->notify != NULL){
              wcolor_set(auswin, 20, NULL);
              beep();
            }

            /* die eigentlich ausgabe */
            char *msgusername=cutName(msgconfig->username);
            if(! msgusername) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 1;
            }
                         
            wprintw(auswin, "%s: %s\n", ccUtf8ToIso(&msgusername), ausgabetext);
            /* ins logfile schreiben */
            if(config->logfile){
              FILE *logfile = fopen(config->logfile, "a");
              if(!logfile)
                wprintw(auswin, "FEHLER beim schreiben ins Logfile\n");
              else{
                fprintf(logfile, "%s: %s\n", msgusername, ausgabetext);
                fclose(logfile);
              }
            }
            /* ans skript senden */
            if(script && *script){
              struct stat filestatbuffer;
              int statback = stat((char*)*script,&filestatbuffer);
              
              if (statback == 0 && (filestatbuffer.st_mode & 0111) == 0111){
                
              FILE *scriptpipe = popen((char*)*script, "w");
                if(!scriptpipe)
                  wprintw(auswin, "FEHLER Öffnen des Skripts\n");
                else{
                  fprintf(scriptpipe, "%s\n", ausgabetext);
                  pclose(scriptpipe);
                }
              }

            }
  
            wrefresh(auswin);
            free(msgusername);
            free(msgconfig);
          }
          
          free(ausgabetext);
          int clients = 0;
          wmove(statwin,0,0);
          wclrtoeol(statwin);
          while((pc->clients[clients]).host != 0){
            if(!(pc->clients[clients].notify != NULL && strcmp(pc->clients[clients].notify, "pong") != 0 &&  strcmp(pc->clients[clients].notify, "online") != 0 &&  strcmp(pc->clients[clients].notify, "rename") != 0)){
            wcolor_set(statwin, clients%9, NULL);
            char *clusername = cutName(pc->clients[clients].username);
            if(! clusername) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 1;
            }
            wprintw(statwin,"%s ",  ccUtf8ToIso(&clusername));
            free(clusername);
            } 
            clients++;
          }
          wrefresh(statwin);
        }
      }
    }

      if(chok  == OK && wch != 13){
        int y = 0;
        int x = 0;
        getyx(einwin,y,x);
        if(x < COLS-1){
          cchar_t my_wchar;
          setcchar(&my_wchar, (wchar_t *) &wch, 0, 0, NULL);

          wins_wch(einwin, &my_wchar);
          wmove(einwin,y,x+1);
        }
        if(x >= COLS-1){
          Lwch = wch;
          isLwch = 1;
        
        
        }
        if(helpwin){
          delwin(helpwin);
          helpwin = NULL;
          touchwin(auswin);
          wrefresh(auswin);
        }
      }
      if(wch == KEY_RESIZE){
        wresize(auswin, LINES-2,COLS);
        if(LINES > lastLines){
          winsdelln(auswin,LINES - lastLines + 2);
        }
        delwin(einwin);
        einwin = subwin(stdscr, 1,COLS,LINES-2,0);
        delwin(statwin);
        statwin = subwin(stdscr, 1,COLS,LINES-1,0);

        wmove(einwin,0,0);
        wclrtoeol(einwin);
        mvwaddstr(einwin, 0, 0, "--> ");
        wmove(einwin,orgy,orgx);
        intrflush(einwin, FALSE);
        keypad(einwin, TRUE);
        
        wrefresh(statwin);
        wrefresh(auswin);
        wrefresh(einwin);
      }
     
    /* letztes zeichen löschen wenn backspace gedrückt wurde */
    if(wch == KEY_BACKSPACE){
      int y = 0;
      int x = 0;
      getyx(einwin,y,x);
      if(x > orgx){
        wmove(einwin,y,x-1);
      }
      wdelch(einwin);
    }
    if(wch == KEY_LEFT){
      int y = 0;
      int x = 0;
      getyx(einwin,y,x);
      if(x > orgx){
        wmove(einwin,y,x-1);
      }
    }
    if(wch == KEY_RIGHT){
      int y = 0;
      int x = 0;
      getyx(einwin,y,x);
      if(x < COLS){
        wmove(einwin,y,x+1);
      }
    }
    if(wch == KEY_DC){
      wdelch(einwin);
    }
    if(wch == KEY_HOME){
      wmove(statwin, 0,0);
      wclrtoeol(statwin);
      wprintw(statwin,"Key: %s", userkey);
      wrefresh(statwin);
    }      

    if(wch == KEY_UP && isAzeile == 0){
      free(Azeile);
      int textalloclen = 500;
      Azeile = calloc(textalloclen+1,1);
      if(! Azeile) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 1;
      }
      mvwinstr(einwin, orgy,orgx, Azeile);
      while(textalloclen >= 0){
        if(Azeile[textalloclen] != '\0' && Azeile[textalloclen] != ' '){
          break;
        }
        Azeile[textalloclen] = '\0';
        textalloclen--;
      }     

      isAzeile = 1;
      wmove(einwin,orgy,orgx);
      wclrtoeol(einwin);
      waddstr(einwin,  Lzeile);
    }
    
    if(wch == KEY_DOWN && isAzeile != 0){
      free(Lzeile);
      int textalloclen = 500;
      Lzeile = calloc(textalloclen+1,1);
      if(! Lzeile) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 1;
      }
      mvwinstr(einwin,  orgy,orgx, Lzeile);
      while(textalloclen >= 0){
        if(Lzeile[textalloclen] != '\0' && Lzeile[textalloclen] != ' '){
          break;
        }
        Lzeile[textalloclen] = '\0';
        textalloclen--;
      }     

      isAzeile = 0;
      wmove(einwin,orgy,orgx);
      wclrtoeol(einwin);
      waddstr(einwin,  Azeile);
    }
    

    /* bei shift+enter oder enter die daten vom win einlesen,
    verschluesseln und auf alle bcast adressen senden */
    if(wch == KEY_ENTER || wch == 13 || isLwch == 1){
      /* zeichen vom win lesen */
      int y = 0;
      int x = 0;
      getyx(einwin,y,x);
      int textalloclen = 500;
      char *texteingabe = calloc(textalloclen+1,1);
      char *letzteswort;
      if(! texteingabe) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 1;
      }
      mvwinstr(einwin, orgy, orgx, texteingabe);
      //wprintw(auswin, "textlen:"); 
      if(isLwch == 1){
        while(textalloclen >= 0){
          if(texteingabe[textalloclen] != '\0'){
            break;
          }
          textalloclen--;
        }     
        //textalloclen--;
        while(textalloclen >= 0){
          if(texteingabe[textalloclen] == ' '){
            break;
          }
          textalloclen--;
        } 
        if(textalloclen > 0){
          texteingabe[textalloclen] = '\0';    
          letzteswort = strdup(&texteingabe[textalloclen+1]);
        }
      }else{
        while(textalloclen >= 0){
          if(texteingabe[textalloclen] != '\0' && texteingabe[textalloclen] != ' '){
            break;
          }
          texteingabe[textalloclen] = '\0';
          textalloclen--;
        }     
      }
   /* wenn userkey da ist dann daten senden 
      ansonsten als key verwenden */
      if(userkey){
        if(strlen(texteingabe) > 0){
          free(Lzeile);
          Lzeile = strdup(texteingabe);
          if(! Lzeile) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 1;
          }
           isAzeile = 0;
          /* eingabe auf '/' als kommandoprefix prüfen und ggf kommando ausführen */
          if(texteingabe[0] == '/'){
            wcolor_set(auswin, 20, NULL);
            /* kommando verarbeiten */
            
            /* profil wechseln bzw neu erstellen */
            if(strncmp(texteingabe,"/profile",8) == 0){
              configrw(WRITE_CONFIG, config);
              if(strlen(texteingabe) < 10){
                  free(config->profilename);
                  config->profilename = strdup(defaultprofile);
                  if(! config->profilename) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
                  }
                }else{
                  free(config->profilename);
                  config->profilename = strdup(texteingabe+9);
                  if(! config->profilename) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
                  }
                }
                struct ChatConfig *configneu = copyChatConfig(config);
                if(! configneu)
                  return 1;
                int configneugeladen =  configrw(READ_CONFIG, configneu);
                if(configneugeladen){
                  /* bin weg senden */
                  struct BinPack sendtext;
                  config->notify=strdup("offline");
                  packtext("habe Quit gemacht", &sendtext, pc);
                  chatsend(&sendtext, pc, NULL);
                  free(sendtext.daten);

                  freeChatConfig(config);
                  config = configneu;
                  pc->chatConfig = configneu;
                  reloadHostlist(pc);
                  userkey = config->userkey;
                  makemd5(userkey, &keys);
                  /* bin da senden */
                  if( config->hostlistlen > 0){
                    wprintw(auswin, "Extrahosts: %d. ", config->hostlistlen);
                  }
                  if(config->logfile){
                    wprintw(auswin, "Logfile: %s, ", config->logfile);
                  }
                  wprintw(auswin, "zu Profil: %s gewechselt \n", config->profilename);

                  config->notify=strdup("online");
                  packtext("Bin auf Empfang", &sendtext, pc);
                  chatsend(&sendtext, pc, NULL);
                  free(sendtext.daten);

                }else{
                  wprintw(auswin, "Kopiere Profil zu: %s\n", config->profilename);
                }
              }

             /* den chat verlassen */
             if(strncmp(texteingabe,"/quit",2) == 0){
              /* bin weg senden */  
              struct BinPack sendtext;
              config->notify=strdup("offline");
              packtext("habe Quit gemacht", &sendtext, pc);
              chatsend(&sendtext, pc, NULL);
              free(sendtext.daten);
              break;
            }
            
            /* neuen key festlegen */
            if(strlen(texteingabe) > 5 && strncmp(texteingabe,"/key",4) == 0){
              userkey = strdup(texteingabe+5);
              if(! userkey) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
              }
              ccIsoToUtf8(&userkey);
              config->userkey = strdup(userkey);
              if(! config->userkey) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
              }
              makemd5(config->userkey, &keys);
	       
              /* bin da senden */  
              struct BinPack sendtext;
              config->notify=strdup("online");
              packtext("Bin auf Emfpang", &sendtext, pc);
              chatsend(&sendtext, pc, NULL);
              free(sendtext.daten);
              configrw(WRITE_CONFIG, config);
              waddstr(auswin, "Key/Raum gespeichert\n");
            }
            if(strlen(texteingabe) == 5 && strncmp(texteingabe,"/host",5) == 0){
              int i;
              for(i = 0;i < config->hostlistlen;i++){
                wprintw(auswin, "Host %d: %s\n",i,config->hostlist[i]);
              }

            }

            /* neuen extrahost zur liste hinzufügen */
            if(strlen(texteingabe) > 6 && strncmp(texteingabe,"/host",5) == 0){
              char *hostname = strdup(texteingabe+6);
              if(! hostname) {
                  fprintf(stderr, "alloc schlug fehl.\n");
                  return 1;
              }
              struct hostent *hostdaten;
              hostdaten = gethostbyname(hostname);
              if(hostdaten){
                waddstr(auswin, "host gefunden:");
                waddstr(auswin, hostname);
                waddstr(auswin, "\n");
                config->hostlist = realloc(config->hostlist, sizeof(char *) * (config->hostlistlen + 1));
                if(! config->hostlist) {
                  fprintf(stderr, "alloc schlug fehl.\n");
                  return 1;
                }

                config->hostlist[config->hostlistlen] = malloc(strlen(hostname) + 1);
                if(! config->hostlist[config->hostlistlen]) {
                  fprintf(stderr, "alloc schlug fehl.\n");
                  return 1;
                }

                *config->hostlist[config->hostlistlen] = '\0';
                strcat(config->hostlist[config->hostlistlen],hostname);
                
                config->hostlistlen++;
                struct in_addr aktuelleAddr;
                aktuelleAddr = *(struct in_addr*) hostdaten->h_addr;
                addTempHost(&aktuelleAddr, pc);

              }
              free(hostname);
            }  

            /* logfile entfernen */
            if(strlen(texteingabe) == 8 && strncmp(texteingabe,"/logfile",8) == 0){
              if(config->logfile){
                free(config->logfile);
                config->logfile = NULL;
              }
              wprintw(auswin, "Logfile gelöscht\n");
            }
 
            /* neues logfile festlegen */
            if(strlen(texteingabe) > 9 && strncmp(texteingabe,"/logfile",8) == 0){
              char *logfilename =  strdup(texteingabe+9);
              if(! logfilename) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
              }
              if(logfilename[0] == '~'){
                char *lfn_erw = strdup(getenv("HOME"));
                if(! lfn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                lfn_erw = realloc(lfn_erw, strlen(lfn_erw)+strlen(logfilename));
                if(! lfn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                strcat(lfn_erw, logfilename+1);
                free(logfilename);
                logfilename = lfn_erw;
              }
              FILE *logfile = fopen(logfilename, "a");
              if(!logfile)
                wprintw(auswin, "Konnte Logfile: %s nicht anlegen!\n", logfilename);
              else{
                fclose(logfile);
                if(config->logfile)
                  free(config->logfile);
                config->logfile = strdup(logfilename);
                if(! config->logfile) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
                }
                wprintw(auswin, "Logfile ist jetzt: %s\n", config->logfile);
              }
              free(logfilename);
            }
            /* neues skript festlegen oder entfernen */
            if(strncmp(texteingabe,"/script",7) == 0){
              if(strlen(texteingabe) > 10){
              char *filename =  strdup(texteingabe+8);
              if(! filename) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
              }
              if(filename[0] == '~'){
                char *fn_erw = strdup(getenv("HOME"));
                if(! fn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                fn_erw = realloc(fn_erw, strlen(fn_erw)+strlen(filename));
                if(! fn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                strcat(fn_erw, filename+1);
                free(filename);
                filename = fn_erw;
              }
              if(!script && scriptNode){
                xmlNode *textnode = xmlNewText((xmlChar*)"");
                script = &textnode->content;
                xmlAddChild(scriptNode, textnode);
              }
              if(script && *script)
                free(*script);
              *script = (xmlChar *)filename;
              wprintw(auswin, "Skript ist jetzt: %s\n", filename);
              }else{
                if(script && scriptNode && scriptNode->children){
                  xmlUnlinkNode(scriptNode->children);
                  wprintw(auswin, "Skript gelöscht\n");
                }
              }
            }
            /* neues skript festlegen oder entfernen */
            if(strncmp(texteingabe,"/oggdir",7) == 0){
              if(strlen(texteingabe) > 8){
              char *filename =  strdup(texteingabe+8);
              if(! filename) {
                    fprintf(stderr, "alloc schlug fehl.\n");
                    return 1;
              }
              if(filename[0] == '~'){
                char *fn_erw = strdup(getenv("HOME"));
                if(! fn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                fn_erw = realloc(fn_erw, strlen(fn_erw)+strlen(filename));
                if(! fn_erw) {
                      fprintf(stderr, "alloc schlug fehl.\n");
                      return 1;
                }
                strcat(fn_erw, filename+1);
                free(filename);
                filename = fn_erw;
              }
              if(!oggdir && oggdirNode){
                xmlNode *textnode = xmlNewText((xmlChar*)"");
                oggdir = &textnode->content;
                xmlAddChild(oggdirNode, textnode);
              }
              if(oggdir && *oggdir)
                free(*oggdir);
              *oggdir = (xmlChar *)filename;
              wprintw(auswin, "Oggfile Verzeichnis ist jetzt: %s\n", filename);
              }else{
                if(oggdir && oggdirNode && oggdirNode->children){
                  xmlUnlinkNode(oggdirNode->children);
                  wprintw(auswin, "Skript gelöscht\n");
                }
              }
            }

            /* hostliste löschen */
            if(strncmp(texteingabe,"/rm",3) == 0){
              
              while(config->hostlistlen > 0){
                free(config->hostlist[config->hostlistlen-1]);
                config->hostlistlen--;
              }
              waddstr(auswin, "hosts geleert\n");
            }

            /* neuen nick festlegen */
            if(strlen(texteingabe) > 6 && strncmp(texteingabe,"/nick",5) == 0){
              
              char *oldnick = strdup(config->username);
              config->username = realloc(config->username, strlen(texteingabe)-4);
              if(! config->username) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
              }
              memcpy(config->username, texteingabe+6,strlen(texteingabe)-4);
              ccIsoToUtf8(&config->username);
              
              char *meldung = strdup("Namenswechsel von: ");
              if(! meldung) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
              }
              ccIsoToUtf8(&meldung);
                            
              char *msg = malloc(strlen(meldung)+strlen(oldnick)+1);
              if(! msg) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
              }
              
              strcpy(msg,meldung);
              strcat(msg,oldnick);
              struct BinPack sendtext;
              config->notify=strdup("rename");
              packtext(msg, &sendtext, pc);
              chatsend(&sendtext, pc, NULL);
              free(sendtext.daten);
              free(meldung);
              free(msg);
            }


            /* ping an alle senden */  
            if(strncmp(texteingabe,"/ping",5) == 0){
              Cping(pc);
            }

            /* chathilfe anzeigen */
            if(strncmp(texteingabe,"/help",5) == 0){
              helpwin = newwin(12,COLS-4, 2, 2);
              box(helpwin, ACS_VLINE, ACS_HLINE);
              
              mvwprintw(helpwin,1,1,"Die Hilfe");
              mvwprintw(helpwin,2,1,"/nick neuer Nick");
              mvwprintw(helpwin,3,1,"/key  neuer Key/Raum / Key: %s", userkey);
              mvwprintw(helpwin,4,1,"/host neuen Host hinzufgen oder alle anzeigen");
              mvwprintw(helpwin,5,1,"/rm Hostliste leeren");
              mvwprintw(helpwin,6,1,"/profile Profil wechseln / Profil: %s", config->profilename);
              mvwprintw(helpwin,7,1,"/ping Onlinanfrage an alle senden");
              char *logfilename = " ";
              if(config->logfile)
                logfilename = config->logfile;
              mvwprintw(helpwin,8,1,"/logfile Nachrichten in Logdatei schreiben / %s", logfilename);
              char *scriptname = " ";
              if(script && *script)
                scriptname = (char*)*script;
              mvwprintw(helpwin,9,1,"/script Nachrichten an Programm senden / %s", scriptname);
              char *oggdirname = " ";
              if(oggdir && *oggdir)
                oggdirname = (char*)*oggdir;  
              mvwprintw(helpwin,10,1,"/oggdir Verzeichnis der Oggfiles / %s", oggdirname);
              wrefresh(helpwin);
            }

            wcolor_set(auswin, 0, NULL);
          }else{
            char *texteingabedup = strdup(texteingabe);
            if(! texteingabedup) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 1;
            }

            ccIsoToUtf8(&texteingabedup);
            struct BinPack sendtext;
            packtext((char *)texteingabedup, &sendtext, pc);
            free(texteingabedup);
            
            
            /* auf die sockets senden */
            chatsend(&sendtext, pc, NULL);
      
            /* speicher freigeben */
            free(sendtext.daten);
          }
        }
      }else{
        /* eingabe als key/raumname verwenden */
        if(strlen(texteingabe) > 0){
          userkey = strdup(texteingabe);
          if(! userkey) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 1;
          }
          ccIsoToUtf8(&userkey);
          config->userkey = strdup(userkey);
          if(! config->userkey) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 1;
          }
          makemd5(userkey, &keys);
          
          /* bin da senden */  
          struct BinPack sendtext;
          config->notify=strdup("online");
          packtext("Bin auf Emfpang", &sendtext, pc);
          chatsend(&sendtext, pc, NULL);
          free(sendtext.daten);
          //userkey = keys.key_md5;
        }else{
          continue;
        }
      }
      
      free(texteingabe);
      /* eingabe zeile loeschen und neu initialisieren*/
      wmove(einwin,0,0);
      wclrtoeol(einwin);
      mvwaddstr(einwin, 0, 0, "--> ");
      if(isLwch == 1){
        waddstr(einwin, letzteswort);
        free(letzteswort);
          cchar_t my_wchar;
          setcchar(&my_wchar, (wchar_t *) &Lwch, 0, 0, NULL);
          wadd_wch(einwin, &my_wchar);
          isLwch = 0;
      }
      wrefresh(einwin);
    }
    chok = ERR;
  }
#ifdef _THREAD
  if(playthreadrun){
    ple_z.oggdir = NULL;
    pthread_cond_broadcast(&ple_z.cond);
    
    //int prc = pthread_cancel(player_thread);
    //fprintf(stderr, "prc: %d\n", prc);
    pthread_detach(player_thread);
    //pthread_join(player_thread, NULL);
  }
  pthread_cond_destroy(&ple_z.cond);
  pthread_mutex_destroy(&ple_z.mutex);
#endif

#ifdef _FORK
  fclose(wpipe);
  close(pipefiled[1]);

#endif
  configrw(WRITE_CONFIG, config);
  /* finisher aufrufen */
  delwin(einwin);
  delwin(auswin);
  delwin(statwin);
  endwin();
  //finisher(0);
  return 0;
}


char *ccIsoToUtf8(char **text){
  if(strcmp(nl_langinfo(CODESET),"UTF-8") != 0){
    int textLen = strlen(*text);
    int utfTextLen = textLen * 4;
    char *utfText = calloc(utfTextLen, 1);
    if(! utfText) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return NULL;
    }
    if(isolat1ToUTF8((unsigned char*)utfText, &utfTextLen,  (unsigned char*)*text, &textLen) == 0){
      free(utfText); 
      return NULL; 
    }
    free(*text);
    *text = utfText;
    return utfText;
  }else{
    return *text;  
  }
}

char *ccUtf8ToIso(char ** text){
  if(strcmp(nl_langinfo(CODESET),"UTF-8") != 0){
    int textLen = strlen(*text)+1;
    int utfTextLen = textLen;
    char *latinText = calloc(textLen,1);
    if(! latinText) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return NULL;
     }
     /* nach utf8 konvertieren */
     UTF8Toisolat1((unsigned char*)latinText, &textLen, (unsigned char*)*text, &utfTextLen);
      free(*text);
      *text = latinText;
      return latinText;
    }else{
      return *text;
  }
}

char *cutName(const char *name){
  char *cutname;
  int maxlen = COLS / 8;
  if(strcmp(nl_langinfo(CODESET),"UTF-8") != 0){
    int len = strlen(name);
    if(len > maxlen){
      cutname = strndup(name, maxlen);
      if(cutname)
        cutname[strlen(cutname)] = '_';
    }else{
      cutname = strdup(name);
    }
  }else{
    int len = xmlUTF8Strlen((xmlChar *)name);
    if(len > maxlen){
    cutname = (char *) xmlUTF8Strndup((xmlChar *)name, maxlen-1);
    int neulen = strlen(cutname);
    if(cutname){
      cutname = realloc(cutname, neulen+1);
      cutname[neulen] = '_';
      cutname[neulen+1] = '\0';
    }
    }else{
      cutname = strdup(name);
    }
    
  }
  return cutname;
}


void
    finisher(int sig)
{
  /* compiler-warnung, dass sig nicht verwendet wird, ignorieren.
  *    * auf grund dieser anweisung wird kein code im binary erstellt!!
  *       */
  (void) sig;

	  
  /* curses stoppen */
  
  endwin();
  exit(0);
}

