/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#ifndef _OGGERER_H
#define _OGGERER_H


struct oggmatch;
struct oggmatch {
  struct oggmatch *next;
  const char *filename;
};

int playinit();

int playshutdown();

int playfile(const char *name);

int parsefiles(const char *name, const char *text, struct oggmatch **list);

int oggerer(const char *dirname, const char *text);


#endif
