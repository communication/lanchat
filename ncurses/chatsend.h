/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include <netdb.h>
#include "chatconfig.h"
#include "protokoll.h"

/* chatdaten bzw text senden 
   der inhalt von *fromclient muss mit free freigegeben werden 
*/
int chatsend(struct BinPack *daten, struct ProtConfig *pc, struct ClientConfig *fromclient);

/* daten vom socket abholen und in *daten zurückgeben
   passende clientconfig erstellen und füllen 
   Clientconfig und inhalt muss mit free freigegeben werden */
struct ClientConfig *chatrecv(struct BinPack *daten, int rxsocket);

/* für die hostliste in der config in aktuelle adressen
   abfragen und in den cache eintragen */ 
int reloadHostlist(struct ProtConfig *pc);

/* adresse auf vorhanden sein testen und zum cache hinzufügen */
int addTempHost(struct in_addr *aktuelleAddr, struct ProtConfig *pc);

/* netzmasken und broadcast adressen ermitteln */ 
int getbcast(int port, struct sockaddr_in **bcastaddr,  struct sockaddr_in **nmaskaddr);

/* test ob der absender ich selbst war */
int binich(unsigned long aktuellerHost);

