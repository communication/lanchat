/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* die config aller sockets holen und in ifc ablegen */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "chatsend.h"
#define BUF 1024

/* chatdaten bzw text senden */
int chatsend(struct BinPack *daten, struct ProtConfig *pc,struct ClientConfig *fromclient){
  struct ChatConfig *config = pc->chatConfig;
  struct sockaddr_in *bcastaddr = NULL;
  struct sockaddr_in *nmaskaddr = NULL;
  
  /* wenn ein paket schon von mir kommt dann nicht senden */
  if(fromclient != NULL && binich(fromclient->host) == 1)
    return 0;

  /* aktuelle broadcast adressen holen */
  int adressenanzahl = getbcast(config->port, &bcastaddr, &nmaskaddr);
	
  /* port auf broadcast schalten */
  int i_opt = 1;
  setsockopt(pc->txsock,SOL_SOCKET,SO_BROADCAST,(void*) &i_opt, sizeof( i_opt ) );
  
  /* daten an jede broadcast adresse senden */
  int i;
  for (i = 0; i < adressenanzahl;i++){
    /* wenn fromclient existent ist dann empfangsdevice ausschliessen */
    if(fromclient == NULL){
      int rc = sendto (pc->txsock, daten->daten, daten->len, 0, (struct sockaddr *) &bcastaddr[i], sizeof (struct sockaddr_in));
      if (rc < 0) {
        fprintf (stderr, "Konnte Daten nicht auf Broadcast senden\n");
        return 0;
      }
    }else{
      if(((htonl(fromclient->host) ^ bcastaddr[i].sin_addr.s_addr) & nmaskaddr[i].sin_addr.s_addr) != 0){
        int rc = sendto (pc->txsock, daten->daten, daten->len, 0, (struct sockaddr *) &bcastaddr[i], sizeof (struct sockaddr_in));
        if (rc < 0) {
          fprintf (stderr, "Konnte Daten nicht auf Broadcast senden\n");
          return 0;
        }
      }
    }
  }
  
  /* broadcast am port ausschalten */
  i_opt = 0;
  setsockopt(pc->txsock,SOL_SOCKET,SO_BROADCAST,(void*) &i_opt, sizeof( i_opt ) );  
  i = 0;
  /* daten an jeden extrahost senden */
  while((pc->tempHostlist[i]).s_addr != 0){
      struct sockaddr_in addr;
      addr.sin_addr = pc->tempHostlist[i];
      addr.sin_family = AF_INET;
      addr.sin_port = htons(config->port);
      /* wenn fromclient existent ist dann empfangsadresse ausschliessen */
      if(fromclient == NULL){
        int rc = sendto (pc->txsock, daten->daten, daten->len, 0, (struct sockaddr *) &addr, sizeof (struct sockaddr_in));
        if (rc < 0) {
          fprintf (stderr, "Konnte keine Daten senden\n");
          return 0;
        }
      }else{
        if(ntohl(fromclient->host) != addr.sin_addr.s_addr){
          int rc = sendto (pc->txsock, daten->daten, daten->len, 0, (struct sockaddr *) &addr, sizeof (struct sockaddr_in));
          if (rc < 0) {
            fprintf (stderr, "Konnte Daten nicht auf Broadcast senden\n");
            return 0;
          }
        }
    }
    i++;
  }
  free(bcastaddr);
  free(nmaskaddr);
	return 1;
}

/* test ob der absender ich selbst war */
int binich(unsigned long aktuellerHost){
  struct ifreq *ifr = NULL;
  struct ifconf ifc;
  struct in_addr me_addr;
  char devbuff[2048];
  int rawsock;
    
  /* interface conf struct initialisieren */
  ifc.ifc_len = sizeof(devbuff);
  ifc.ifc_buf = devbuff;
  /* socket besorgen */
  rawsock = socket (AF_INET, SOCK_DGRAM, 0);
  if (rawsock < 0) {
    fprintf(stderr, "Konnte Socket nicht oeffnen");
    return(-1);
  }
  
  if (ioctl(rawsock, SIOCGIFCONF, &ifc) < 0) {
    perror("ioctl SIOCGIFCONF");
    return(-1);
  }
  /* den zeiger auf das erste devices in ifr ablegen */
  ifr = ifc.ifc_req;
  /* nach dev adressen suchen und auf gleichheit prfen */
  int i;
  for (i = ifc.ifc_len / sizeof(struct ifreq); --i >= 0; ifr++) {
    /* printf("Interfacename: %s\n", ifr->ifr_ifrn.ifrn_name); */

    if (ioctl(rawsock, SIOCGIFADDR, ifr) < 0) {
      perror("ioctl SIOCGIFADDR");
      return(0);
    }
    me_addr = ((struct sockaddr_in *) &(ifr->ifr_ifru.ifru_addr))->sin_addr;
    //printf(stderr, "fromhost: %s\n", inet_ntoa(aktuelleAddr));
    //printf(stderr, "ichbin: %s\n", inet_ntoa(me_addr));
    
    if(me_addr.s_addr == htonl(aktuellerHost))
      return 1;
  }

  close(rawsock);
  return 0;
}

/* für die hostliste in der config in aktuelle adressen
   abfragen und in den cache eintragen */ 
int reloadHostlist(struct ProtConfig *pc){
  int i;
  for (i = 0; i < pc->chatConfig->hostlistlen;i++){
    //fprintf(stderr, "host: %s\n", pc->chatConfig->hostlist[i]);

    struct hostent *hostdaten;
    hostdaten = gethostbyname(pc->chatConfig->hostlist[i]);
    if(hostdaten){
      struct in_addr aktuelleAddr;
      aktuelleAddr = *(struct in_addr*) hostdaten->h_addr;
      addTempHost(&aktuelleAddr, pc);
    }
  }
  return 1;
}

/* adresse auf vorhanden sein testen und zum cache hinzufügen */
int addTempHost(struct in_addr *aktuelleAddr, struct ProtConfig *pc){
  struct sockaddr_in *bcastaddr = NULL;
  struct sockaddr_in *nmaskaddr = NULL;
  int adressenanzahl = getbcast(pc->chatConfig->port, &bcastaddr, &nmaskaddr);

  int clients = 0;
  int clientda = 0;
  while((pc->tempHostlist[clients]).s_addr != 0){
    if((pc->tempHostlist[clients]).s_addr == aktuelleAddr->s_addr) {
      clientda = 1;
      break;
    }
    clients++;
  }
  int i;
  for (i = 0; i < adressenanzahl;i++){
    if(((aktuelleAddr->s_addr ^ bcastaddr[i].sin_addr.s_addr) & nmaskaddr[i].sin_addr.s_addr) == 0){
      clientda = 1;  
    }  
  }  
  /* bei neuem client den speicher entsprechend erweitern */
  if(!clientda){
    pc->tempHostlist = realloc(pc->tempHostlist ,sizeof(struct in_addr) * (clients + 2));
    if(! pc->tempHostlist) {
              fprintf(stderr, "alloc schlug fehl.\n");
              return 0;
    }
    memset(&(pc->tempHostlist[clients+1]),0,sizeof(struct in_addr));
    memcpy(&(pc->tempHostlist[clients]),&(aktuelleAddr->s_addr), sizeof(struct in_addr));
  }
  free(bcastaddr);
  free(nmaskaddr);
  return 1;
}

/* netzmasken und broadcast adressen ermitteln */ 
int getbcast(int port, struct sockaddr_in **bcastaddr, struct sockaddr_in **nmaskaddr){
  struct ifreq *ifr = NULL;
  struct ifconf ifc;
  struct in_addr bc_addr, nm_addr;
  int devanzahl = 0;
  char devbuff[2048];
  int rawsock;
	  
  /* interface conf struct initialisieren */
  ifc.ifc_len = sizeof(devbuff);
  ifc.ifc_buf = devbuff;
  rawsock = socket (AF_INET, SOCK_DGRAM, 0);
  if (rawsock < 0) {
    fprintf(stderr, "Konnte Socket nicht oeffnen");
    return(0);
  }
  
  if (ioctl(rawsock, SIOCGIFCONF, &ifc) < 0) {
    perror("ioctl SIOCGIFCONF");
    return(0);
  }
  /* den zeiger auf das erste devices in ifr ablegen */
  ifr = ifc.ifc_req;
  /* nach broadcast adressen suchen und auf gueltigkeit filtern
   *   alle gueltigen in remoteServAddr[] ablegen */
  int i;
  for (i = ifc.ifc_len / sizeof(struct ifreq); --i >= 0; ifr++) {

    if (ioctl(rawsock, SIOCGIFBRDADDR, ifr) < 0) {
      perror("ioctl SIOCGIFBRDADDR");
      return(0);
    }
    bc_addr = ((struct sockaddr_in *) &(ifr->ifr_ifru.ifru_broadaddr))->sin_addr;
    if (ioctl(rawsock, SIOCGIFNETMASK, ifr) < 0) {
        perror("ioctl SIOCGIFNETMASK");
        return(0);
    }
    nm_addr = ((struct sockaddr_in *) &(ifr->ifr_ifru.ifru_netmask))->sin_addr;
    
    if((bc_addr.s_addr > 0) && ((bc_addr.s_addr & 255) != 127)){
      (*bcastaddr) = realloc((*bcastaddr), sizeof(struct sockaddr_in) * (devanzahl + 1));
      if(! (*bcastaddr)) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
      }
              
      (*bcastaddr)[devanzahl].sin_family = AF_INET;
      (*bcastaddr)[devanzahl].sin_addr = bc_addr;
      (*bcastaddr)[devanzahl].sin_port = htons(port);

      (*nmaskaddr) = realloc((*nmaskaddr), sizeof(struct sockaddr_in) * (devanzahl + 1));
      if(! (*nmaskaddr)) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
      }
              
      (*nmaskaddr)[devanzahl].sin_family = AF_INET;
      (*nmaskaddr)[devanzahl].sin_addr = nm_addr;
      (*nmaskaddr)[devanzahl].sin_port = htons(port);

      devanzahl++;
			     
    }
  }
  close(rawsock);
  return(devanzahl);
}

/* daten vom socket abholen und in *daten zurückgeben
   passende clientconfig erstellen und füllen */
struct ClientConfig *chatrecv(struct BinPack *daten, int rxsocket){
  struct ClientConfig *config = initClientConfig();
  if(! config)
    return NULL;
  struct sockaddr_in cliAddr;
  socklen_t len = sizeof (cliAddr);
  daten->len = recvfrom ( rxsocket, daten->daten, BUF, 0,
                                 (struct sockaddr *) &cliAddr, &len );
  config->port = ntohs(cliAddr.sin_port); 
  config->host = ntohl(cliAddr.sin_addr.s_addr);
  if(daten->len <= 0)
    return NULL;
    
  return config;


}


