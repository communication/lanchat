/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "chatconfig.h"
#include "chatcrypt.h"

#ifndef _PROTOKOL_H
#define _PROTOKOL_H

struct ProtConfig{
  struct ChatConfig *chatConfig;
  struct md5key *keys;
  struct BinPack lastrecv;
  struct BinPack lastsend;
  struct ClientConfig *clients;
  struct in_addr *tempHostlist;
  int txsock; 
};


/*init der protokollumgebung 
  inhalt und die struct selbst muss ggf mit free freigegen werden 
*/
struct ProtConfig *protokollInit(struct ChatConfig *config, struct md5key *keys);

/* verpacken den UTF8 (!!) daten in ein xml doc
   , zlib packen und verschluesseln */
int packtext(const char *text, struct BinPack *daten, struct ProtConfig *pc);

/* entschlusseln, auspacken, und verarbeiten der xmldaten.
   das ganze fortlaufend auf gueltigkeit pruefen und bei bedarf weiterleiten 
   UTF8 wird zurückgegeben 

   *text muss mit free freigeben werden sowie die clientconfig und deren inhalt
*/
int entpacktext(struct BinPack *daten, char **text, struct ClientConfig *config, struct ProtConfig *pc);

/* eine pinganfrage versenden und bei jeder clientconfig vermerken */
int Cping(struct ProtConfig *pc);

/* daten mit zlib paken */
int pack(struct BinPack *uncomp, struct BinPack *comp);

/* daten mit zlib paken */
int unpack(struct BinPack *comp, struct BinPack *uncomp);

#endif
