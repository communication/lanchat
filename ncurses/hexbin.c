/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hexbin.h"

/* die daten zu einen ascii hex string konvertieren */
int binhex(struct BinPack *daten, unsigned char **hexstring){
  int i;
  *hexstring = malloc(daten->len * 2 + 1);
  if(*hexstring == NULL){
    return 0;
  }
  unsigned char h;
  unsigned char l;

  *(*hexstring+((daten->len * 2) )) = '\0';
  for( i=0; i < daten->len; i++){
    l = ((unsigned char)daten->daten[i] & 0x0f) + '0' ;
    if(l > 57) l += 7;
    h = ((unsigned char)daten->daten[i] >> 4) + '0';
    if(h > 57) h += 7;
    (*hexstring)[2*i] = h;
    (*hexstring)[2*i+1] = l;

  }
  return 1;
}


/* hex ascii string in binärdaten konvertieren */
int hexbin(const unsigned char *hexstring, struct BinPack *daten){
  daten->daten = NULL;
  int h;
  int l;
  for(daten->len = 0;daten->len < (((int)(strlen((char *)hexstring)))/2);daten->len++){
    daten->daten = realloc(daten->daten, daten->len +1);
    h = hexstring[2*daten->len] - '0';
    if(h > 15 && h < 23) h -= 7;
    l = hexstring[2*daten->len+1] - '0';
    if(l > 15 && l < 23) l -= 7;
    if( h < 0 || h > 15 || l < 0 || l > 15){
      return 0;
    }
    daten->daten[daten->len] = (unsigned) h * 16 + l;

  }
  return 1;

}

