/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "chatconfig.h"

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/parser.h>
//#include <libxml/tree.h>
#include <libxml/debugXML.h>

#include <zlib.h>

#include "hexbin.h"
#define CONTEXT "lc"
#define CONTEXTURL "http://www.lugrot.de"

/* beschaft eine chatconfig und initialisiert sie */
struct ChatConfig *initChatConfig(const char *profile){
  struct ChatConfig *config = malloc(sizeof(struct ChatConfig));
  if(! config) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return NULL;
  }
  config->port = PORT;
  config->configfilename = NULL;
  config->doc = NULL;
  config->uiconfig = NULL;
  config->logfile = NULL;
  
  config->id = NULL;
  config->username = NULL;
  config->userkey = NULL;
  config->hostlist = NULL;
  config->hostlistlen = 0;
  config->notify = NULL;
  config->uiname = NULL;

  config->profilename = strdup(profile);
  if(! config->profilename) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return NULL;
  }
  config->ns = xmlNewNs(NULL, (unsigned char*)CONTEXTURL, (unsigned char*)CONTEXT);
  if(! config->ns) {
    fprintf(stderr, "unable to register own namespace\n");
    return NULL;
  }
  

  return config;
}

/* kopiert eine chatconfig und gibt die kopie zurück 
   aber ohne die xml daten zu kopieren */
struct ChatConfig *copyChatConfig(struct ChatConfig *orgconfig){
  struct ChatConfig *config = initChatConfig(orgconfig->profilename);
  if(! config) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return NULL;
  }
  config->port = orgconfig->port;
  config->hostlistlen = orgconfig->hostlistlen;

  if(orgconfig->configfilename){
    config->configfilename = strdup(orgconfig->configfilename);
    if(! config->configfilename) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  if(orgconfig->logfile){
    config->logfile = strdup(orgconfig->logfile);
    if(! config->logfile) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  if(orgconfig->id){
    config->id = strdup(orgconfig->id);
    if(! config->id) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  if(orgconfig->username){
    config->username = strdup(orgconfig->username);
    if(! config->username) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  if(orgconfig->userkey){
    config->userkey = strdup(orgconfig->userkey);
    if(! config->userkey) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  if(orgconfig->uiname){
    config->uiname = strdup(orgconfig->uiname);
    if(! config->uiname) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }

  config->notify = NULL;
  config->doc = NULL;
  config->uiconfig = NULL;
  config->ns = xmlNewNs(NULL, (unsigned char*)CONTEXTURL, (unsigned char*)CONTEXT);
  if(! config->ns) {
    fprintf(stderr, "unable to register own namespace\n");
    return NULL;
  }


  config->hostlist = malloc(sizeof(char *) * (config->hostlistlen + 1));
  if(! config->hostlist) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return NULL;
  }
  int i;
  for(i = 0; i < config->hostlistlen; i++){
    config->hostlist[i] = strdup(orgconfig->hostlist[i]);
    //fprintf(stderr, "kopiere host %d: %s\n", i, orgconfig->hostlist[i]);
    if(! config->hostlist[i]) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return NULL;
    }
  }
  return config;
}

/* gibt eine chatconfig wieder frei */
void freeChatConfig(struct ChatConfig *config){
  if(config){
    if(config->logfile)
      free(config->logfile);
    if(config->username)
      free(config->username);
    if(config->id)
      free(config->id);
    if(config->userkey)
      free(config->userkey);
    if(config->configfilename)
      free(config->configfilename);
    if(config->notify)
      free(config->notify);
    if(config->uiname)
      free(config->uiname);
            
    int i;
    for(i = 0; i < config->hostlistlen; i++){
      //fprintf(stderr, "hoslistlen: %d\n", i);
      free(config->hostlist[i]);
    }
    if(config->hostlist)
      free(config->hostlist);
    free(config); 
      
  }
}

/* beschaft eine clientconfig und initialisiert sie */
struct ClientConfig *initClientConfig() {
  struct ClientConfig *config = malloc(sizeof(struct ClientConfig));
  if(! config) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return NULL;
  }

   config->id = NULL;
   config->username = NULL;
   config->room = NULL;
   config->roomlen = 0;
   config->host = 0;
   config->port = 0;
   config->usec = 0;
   config->sec = 0;
   config->notify = NULL;
  
   return config;
}

/* leert den inhalt einer clientconfig */
void freeClientConfigInhalt(struct ClientConfig *config) {
  if(config) {
   if(config->id)
   free(config->id);
   if(config->id)
   free(config->username);
   if(config->id)
   free(config->room);
   if(config->id)
   free(config->notify);
  }  
}

/* liest die daten für eine chatconfig von xmldatei oder
   füllt die werte mit standartwerten aus dem system env 
   bzw schreibt die config in eine xmldatei
  */
int configrw(int rw, struct ChatConfig *config){
  const char *envusername = "LOGNAME";
  int configgeladen = 0;
  config->notify = NULL;
  
  char *xml_conffilepfadname;
  
  if(!config->configfilename){
  char *homedir = getenv("HOME");
  char *conffilename = "/.lanchat.xml";
  
   /*  xml  */
  xml_conffilepfadname = malloc(strlen(conffilename)+strlen(homedir)+1);
  if(! xml_conffilepfadname) {
    fprintf(stderr, "alloc schlug fehl.\n");
    return 0;
   }
  *xml_conffilepfadname = '\0';
  strcat(xml_conffilepfadname, homedir);
  strcat(xml_conffilepfadname, conffilename);
  }else{
    xml_conffilepfadname = strdup(config->configfilename);
    if(! xml_conffilepfadname) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return 0;
    }
  }


  /* config einlesen */
  if(rw == READ_CONFIG){
    config->hostlistlen = 0;
    config->hostlist = malloc(sizeof(char *));
    if(! config->hostlist) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return 0;
    }
    
    /* username aus der umgebung holen */
    config->username = strdup(getenv(envusername));
    if(! config->username) {
      fprintf(stderr, "alloc schlug fehl.\n");
      return 0;
    }
    
    struct timeval sendezeit;
    struct timezone tzbuf;
    gettimeofday(&sendezeit, &tzbuf);
    char idbuf[64] = { 0 };
    sprintf(idbuf, "%d%d", (int) sendezeit.tv_sec, (int) sendezeit.tv_usec);
    config->id = strdup(idbuf);

    /* xml */
    xmlKeepBlanksDefault(0);
    //xmlIndentTreeOutput = 0;
    config->doc = xmlParseFile(xml_conffilepfadname);
    if(config->doc) {
      //xmlSaveFormatFile("test.xml", config->doc, 1);
      xmlXPathInit();
      xmlXPathContext *ctx = xmlXPathNewContext(config->doc);
      if(! ctx) {
         //fprintf(stderr, "xmlXPathContext schlug fehl.\n");
        return 0;
      }
      xmlXPathRegisterNs(ctx,(unsigned char*) CONTEXT, (unsigned char*) CONTEXTURL);
      int alloclen = strlen(config->profilename)+strlen(CONTEXT)+50;
      char *id_e = calloc(alloclen, 1);
      char *username_e = calloc(alloclen, 1);
      char *key_e = calloc(alloclen, 1);
      char *logfile_e = calloc(alloclen, 1);
      char *hostlist_e = calloc(alloclen, 1);
      char *pfad_e = calloc(alloclen, 1);
      char *uiconfig_e = calloc(alloclen, 1);

      if(! id_e || !username_e || !key_e || !hostlist_e || !pfad_e || !uiconfig_e) {
        fprintf(stderr, "alloc schlug fehl.\n");
        return 0;
      }
      sprintf(pfad_e,"/%s:lanchat/%s:profile[@name='%s']/%s", CONTEXT, CONTEXT, config->profilename, CONTEXT);
      
      sprintf(id_e,"%s:id/text()", pfad_e);
      sprintf(username_e,"%s:username/text()", pfad_e);
      sprintf(key_e,"%s:key/text()", pfad_e);
      sprintf(logfile_e,"%s:logfile/text()", pfad_e);
      sprintf(hostlist_e,"%s:hostlist/*", pfad_e);
      //sprintf(hostlist_e,"%s:hostlist/%s:host", pfad_e, CONTEXT);
      const char delimiters[] = "\n ";

      if(config->uiname){
        sprintf(uiconfig_e,"/%s:lanchat/%s:uiconfig[@name='%s']", CONTEXT, CONTEXT, config->uiname);
        xmlXPathObject *uiconfig_obj = xmlXPathEvalExpression((unsigned char*)uiconfig_e, ctx);
        //xmlXPathDebugDumpObject(stderr, uiconfig_obj,1); /*debug*/
        if(uiconfig_obj && uiconfig_obj->nodesetval && uiconfig_obj->nodesetval->nodeNr > 0){
          config->uiconfig = uiconfig_obj->nodesetval->nodeTab[0];
          if(config->uiconfig){
            xmlUnlinkNode(config->uiconfig);

          }
        }
      }

      //const xmlChar *id_expr = (unsigned char*)id_e;
      xmlXPathObject *id_obj = xmlXPathEvalExpression((unsigned char*)id_e, ctx);
      if(id_obj) {
        char *configid = (char*) xmlXPathCastToString(id_obj);
        if(strlen(configid)>0){
          //token = strtok (hostlist, delimiters)
          config->id = strdup(strtok(configid,delimiters));        
          if(! config->id) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          configgeladen++;
        }
      }
      
      //const xmlChar *username_expr = (unsigned char*)username_e;
      xmlXPathObject *username_obj = xmlXPathEvalExpression((unsigned char*)username_e, ctx);
      if(username_obj) {
        char *username = (char*) xmlXPathCastToString(username_obj);
        if(strlen(username)>0){
          config->username = strdup(strtok(username,delimiters));   
          if(! config->username) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          configgeladen++;
          //fprintf(stderr, "un:%s\n", username);
          }
      }
      
      //const xmlChar *key_expr = (unsigned char*)key_e;
      xmlXPathObject *key_obj = xmlXPathEvalExpression((unsigned char*)key_e, ctx);
      if(key_obj) {
        char *key = (char*) xmlXPathCastToString(key_obj);
        if(strlen(key)>0){
          config->userkey = strdup(strtok(key,delimiters));
          if(! config->userkey) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          //fprintf(stderr, "len: %d \n", strlen(config->userkey));
          configgeladen++;
        }else{
          config->userkey = NULL;
         }
      }
      
      xmlXPathObject *logfile_obj = xmlXPathEvalExpression((unsigned char*)logfile_e, ctx);
      if(logfile_obj) {
        if(config->logfile)
            free(config->logfile);
        char *logfile = (char*) xmlXPathCastToString(logfile_obj);
        if(strlen(logfile)>0){
          config->logfile = strdup(strtok(logfile,delimiters));
          if(! config->logfile) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          //fprintf(stderr, "len: %d \n", strlen(config->userkey));
          configgeladen++;
        }else{
          config->logfile = NULL;
         }
      }
      
      
      //const xmlChar *hostlist_expr = (unsigned char*)hostlist_e;
      xmlXPathObject *hostlist_obj = xmlXPathEvalExpression((unsigned char*)hostlist_e, ctx);

      if(hostlist_obj && hostlist_obj->nodesetval) {
      //xmlXPathDebugDumpObject(stderr, hostlist_obj,1); /*debug*/
      config->hostlistlen = 0;
      int i;
      for(i = 0;i < hostlist_obj->nodesetval->nodeNr;i++){
        if(hostlist_obj->nodesetval->nodeTab[i] && hostlist_obj->nodesetval->nodeTab[i]->children && hostlist_obj->nodesetval->nodeTab[i]->children->content){
          config->hostlist = realloc(config->hostlist, sizeof(char *) * (config->hostlistlen + 1));
          if(! config->hostlist) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          config->hostlist[config->hostlistlen] = strdup((char *)hostlist_obj->nodesetval->nodeTab[i]->children->content);
          if(! config->hostlist[config->hostlistlen]) {
            fprintf(stderr, "alloc schlug fehl.\n");
            return 0;
          }
          //fprintf(stderr, "%s\n", hostlist_obj->nodesetval->nodeTab[i]->children->content);
          config->hostlistlen++;
          configgeladen++;
        }
      }
    }
       
      free(id_e);
      free(username_e);
      free(key_e);
      free(logfile_e);
      free(hostlist_e);
      free(pfad_e);
     
    }
    
  }
  if(rw == WRITE_CONFIG){
    /* xml */
    if(!config->doc){
      config->doc = xmlNewDoc((unsigned char*) "1.0");
      if(! config->doc) {
        fprintf(stderr, "unable to allocate new xml document\n");
        return 0;
      }
    }
    xmlNode *root = xmlDocGetRootElement(config->doc);
    if(! root){
      root  = xmlNewNode(config->ns, (unsigned char*)"lanchat");
      if(! root) {
        fprintf(stderr, "unable to allocate new xml document\n");
        return 0;
      }
      root->nsDef = config->ns;
      xmlDocSetRootElement(config->doc, root);
    }

    xmlNode *ncurses_root;
    xmlNode *tags = root->children;
    //xmlDebugDumpOneNode(stderr, tags,0);
    if(tags){
    
    while(tags != NULL){
      //xmlDebugDumpOneNode(stderr, tags,0);
      //fprintf(stderr, "sollte ncurses sein: %s\n", (char *)tags->name);
      if(tags->name && (strcmp((char *)tags->name, "profile") == 0)){
        xmlAttr *attr = xmlHasProp(tags, (xmlChar *) "name");
        //fprintf(stderr, "attr: %s\n", (char *)attr->name);
        //fprintf(stderr, "child: %s\n", (char *)attr->children->content);
        //xmlDebugDumpOneNode(stderr, tags,0);
        if(attr->children && attr->children->content && (strcmp((char *)attr->children->content, config->profilename) == 0)){
          xmlUnlinkNode(tags);
          break;
        }
      }
      tags = tags->next;       
      }
    }
    ncurses_root = xmlNewChild(root, config->ns, (unsigned char*)"profile", NULL);
    if(! ncurses_root) {
    fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(ncurses_root,(unsigned char*) "name",(unsigned char*) config->profilename);
    xmlNode *c_key = xmlNewTextChild(ncurses_root, config->ns, (unsigned char*)"key", (unsigned char*)config->userkey);
    if(! c_key) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(c_key,(unsigned char*) "content",(unsigned char*) "text");
    
    if(config->logfile){
      xmlNode *c_logfile = xmlNewTextChild(ncurses_root, config->ns, (unsigned char*)"logfile", (unsigned char*)config->logfile);
      if(! c_logfile) {
        fprintf(stderr, "unable to allocate child node\n");
        return 0;
      }
      xmlNewProp(c_logfile,(unsigned char*) "content",(unsigned char*) "text");
    }
    
      
     xmlNode *c_username = xmlNewTextChild(ncurses_root, config->ns, (unsigned char*)"username", (unsigned char*)config->username);
    if(! c_username) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(c_username,(unsigned char*) "content",(unsigned char*) "text");

    xmlNode *c_id = xmlNewTextChild(ncurses_root, config->ns, (unsigned char*)"id", (unsigned char*) config->id);
    if(! c_id) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(c_id,(unsigned char*) "content",(unsigned char*) "text");
    
    xmlNode *c_hostlist = xmlNewChild(ncurses_root, config->ns, (unsigned char*)"hostlist", NULL);
    if(! c_hostlist) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(c_hostlist,(unsigned char*) "content",(unsigned char*) "list");
    int xi;
    for(xi = 0;xi < config->hostlistlen;xi++){
    
      xmlNode *c_host = xmlNewTextChild(c_hostlist, config->ns, (unsigned char*)"host", (unsigned char*) config->hostlist[xi]);
      if(! c_host) {
        fprintf(stderr, "unable to allocate child node\n");
        return 0;
      }
      xmlNewProp(c_host,(unsigned char*) "content",(unsigned char*) "text");
    
    }
    if(config->uiconfig && config->uiname){
      xmlAddChild(root, config->uiconfig);
    }

    xmlSaveFormatFile(xml_conffilepfadname, config->doc, 1);
  }
  free(xml_conffilepfadname);
  return (configgeladen);
}
