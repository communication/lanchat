/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include <libxml/xpathInternals.h>
 
#define READ_CONFIG 0
#define WRITE_CONFIG 1
#define PORT 23235


#ifndef _CONFIG
#define _CONFIG

struct ChatConfig {
  char *id;
  char *username;
  char *logfile;
  char *configfilename;
  char *userkey;
  char **hostlist;
  int hostlistlen;
  int port;
  char *notify;
  xmlDoc *doc;
  xmlNs *ns;
  char *profilename;
  char *uiname;
  xmlNode *uiconfig;
}; 

struct ClientConfig {
  char *id;
  char *username;
  char *room;
  int roomlen;
  unsigned long host;
  unsigned int port;
  int usec;
  int sec;
  char *notify;
}; 

struct BinPack {
  int len;
  char *daten;
};

/* beschaft eine chatconfig und initialisiert sie 
   sie muss mit freeChatConfig freigegeben werden*/
struct ChatConfig *initChatConfig(const char *profile);

/* kopiert eine chatconfig und gibt die kopie zurück 
   aber ohne die xml daten zu kopieren 
   auch diese muss mit freeChatConfig freigegeben werden*/
struct ChatConfig *copyChatConfig(struct ChatConfig *orgconfig);

/* gibt eine chatconfig wieder frei */
void freeChatConfig(struct ChatConfig *config);

/* beschaft eine clientconfig und initialisiert sie 
   deren inhalt wird mit freeClientConfigInhalt freigegeben
   was normalerweise im protokoll automatisch geschieht 
   die struct clientconfig muss mit free freigegeben werden */
struct ClientConfig *initClientConfig();

/* leert den inhalt einer clientconfig */
void freeClientConfigInhalt(struct ClientConfig *config);

/* liest die daten für eine chatconfig von datei oder
   füllt die werte mit standartwerten aus dem system env
   bzw schreibt die config in eine xmldatei */
int configrw(int rw, struct ChatConfig *config);

#endif
