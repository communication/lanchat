/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "protokoll.h"

#include <sys/types.h>
#include <sys/time.h>

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <libxml/xmlwriter.h>
#include <libxml/tree.h>
#include <sys/param.h>

#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/parser.h>
#include <zlib.h>

#include "hexbin.h"
#include "chatcrypt.h"
#include "chatsend.h"
#define CONTEXT "lc"
#define CONTEXTURL "http://www.lugrot.de"

/* errorhandler um die fehlerausgabe beim verarbeiten der xml
   daten zu unterdruecken */
void xmlErrorHandler(void * userData, xmlErrorPtr error){
        (void) userData;
        (void) error;
}

/*init der protokollumgebung */
struct ProtConfig *protokollInit(struct ChatConfig *config,struct md5key *keys){
  struct ProtConfig *pc = malloc(sizeof(struct ProtConfig));
  if(! pc)
    return NULL;
  pc->chatConfig = config;
  pc->keys = keys;
  pc->txsock = socket (AF_INET, SOCK_DGRAM, 0);
  if (pc->txsock < 0) {
    fprintf(stderr, "Konnte Socket nicht oeffnen");
    return NULL;
  }
  int i_opt = 1;
  setsockopt(pc->txsock, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof( i_opt ));
  struct sockaddr_in servAddr;
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
  servAddr.sin_port = htons (config->port);

  int rc = bind ( pc->txsock, (struct sockaddr *) &servAddr,
              sizeof (servAddr));
  if (rc < 0) {
    fprintf (stderr, "Kann die Portnummern %d beim tx nicht bind(en)\n", config->port);
    return NULL;
  }

  
  
  pc->lastrecv.daten = malloc(1);
  if(! pc->lastrecv.daten) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return NULL;
  }
  pc->lastrecv.len = 1;
  pc->lastsend.daten = malloc(1);
  if(! pc->lastsend.daten) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return NULL;
  }
  pc->lastsend.len = 1;
  pc->clients = malloc(sizeof(struct ClientConfig));
  if(! pc->clients) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return NULL;
  }
  memset(pc->clients, 0, sizeof(struct ClientConfig));
  pc->tempHostlist = malloc(sizeof(struct in_addr));
  if(! pc->tempHostlist) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return NULL;
  }
  memset(pc->tempHostlist, 0, sizeof(struct in_addr));
  return pc;
}

/* verpacken den UTF8 (!!) daten in ein xml doc
   , zlib packen und verschluesseln  */
int packtext(const char *text, struct BinPack *daten, struct ProtConfig *pc){

  xmlDoc *doc = xmlNewDoc((unsigned char*) "1.0");
  if(! doc) {
    fprintf(stderr, "unable to allocate new xml document\n");
    return 0;
  }

  //xmlNs *ns = xmlNewNs(NULL, "http://www.lugrot.de/", NULL);
  xmlNs *ns = xmlNewNs(NULL, (unsigned char*)CONTEXTURL, (unsigned char*)CONTEXT);
  if(! ns) {
    fprintf(stderr, "unable to register own namespace\n");
    return 0;
  }

  xmlNode *root = xmlNewNode(ns, (unsigned char*)"chat");
  if(! root) {
    fprintf(stderr, "unable to allocate new xml document\n");
    return 0;
  }
  root->nsDef = ns;
  xmlDocSetRootElement(doc, root);

  xmlNode *childnodemsg = xmlNewTextChild(root, ns, (unsigned char*)"msg", (unsigned char*)text);
  if(! childnodemsg) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnodemsg,(unsigned char*) "content",(unsigned char*) "text");

  xmlNode *childnodeid = xmlNewTextChild(root, ns, (unsigned char*)"id", (unsigned char*) pc->chatConfig->id);
  if(! childnodeid) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnodeid,(unsigned char*) "content",(unsigned char*) "text");
  
  xmlNode *childnodename = xmlNewTextChild(root, ns, (unsigned char*)"name", (unsigned char*) pc->chatConfig->username);
  if(! childnodename) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnodename,(unsigned char*) "content",(unsigned char*) "text");

  int notify_true = 0;
  if(pc->chatConfig->notify != NULL){
    xmlNode *childnodenotify = xmlNewTextChild(root, ns, (unsigned char*)"notify", (unsigned char*) pc->chatConfig->notify);
    if(! childnodenotify) {
      fprintf(stderr, "unable to allocate child node\n");
      return 0;
    }
    xmlNewProp(childnodenotify,(unsigned char*) "content",(unsigned char*) "text");
    free(pc->chatConfig->notify);
    pc->chatConfig->notify = NULL;
    notify_true = 1;
  }

  struct BinPack room;
  room.len = 16;
  room.daten = pc->keys->room_md5;
  unsigned char *roomhex;
  binhex(&room, &roomhex);

  xmlNode *childnoderoom = xmlNewChild(root, ns,(unsigned char*) "room",(unsigned char*) roomhex);
  if(! childnoderoom) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnoderoom,(unsigned char*) "content",(unsigned char*) "hex");
  char roomlen[32] = { 0 };
  sprintf(roomlen, "%d", room.len);
  xmlNewProp(childnoderoom,(unsigned char*) "len",(unsigned char*) roomlen);
  
  free(roomhex);

  xmlNode *childnodetime = xmlNewChild(root, ns,(unsigned char*) "time", NULL);
  if(! childnodetime) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }

  struct timeval sendezeit;
  struct timezone tzbuf;
  gettimeofday(&sendezeit, &tzbuf);
  char sec[32] = { 0 };
  char usec[32] = { 0 };
  sprintf(sec, "%d", (int) sendezeit.tv_sec);
  sprintf(usec, "%d", (int) sendezeit.tv_usec);

    
  xmlNode *childnodesec = xmlNewChild(childnodetime, ns,(unsigned char*) "sec", (unsigned char*) sec);
  if(! childnodesec) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnodesec,(unsigned char*) "content",(unsigned char*) "int");
  
  xmlNode *childnodeusec = xmlNewChild(childnodetime, ns,(unsigned char*) "usec", (unsigned char*) usec);
  if(! childnodeusec) {
    fprintf(stderr, "unable to allocate child node\n");
    return 0;
  }
  xmlNewProp(childnodeusec,(unsigned char*) "content",(unsigned char*) "int");
  
  xmlChar *mem;
  int memlen;
  xmlDocDumpMemory(doc, &mem, &memlen);
  //xmlSaveFormatFile("chat.xml", doc, 1);
  struct BinPack xml;
  xml.len = memlen;
  xml.daten = (char*) mem;

  /* zusammenpacken */
  struct BinPack gepacktedaten;
  pack(&xml, &gepacktedaten);
  /* verschluesslen */
  Cencrypt(&gepacktedaten, daten, pc->keys);
  
  if(!notify_true){
    pc->lastsend.daten = realloc(pc->lastsend.daten, daten->len);
    if(! pc->lastsend.daten) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
    }
    memcpy(pc->lastsend.daten,daten->daten,daten->len);
    pc->lastsend.len = daten->len;

  }    
  
  free(gepacktedaten.daten);
  xmlFree(mem);

  return 1;
}

/* entschlusseln, auspacken, und verarbeiten der xmldaten.
   das ganze fortlaufend auf gueltigkeit pruefen und bei bedarf weiterleiten 
   UTF8 wird zurückgegeben */
int entpacktext(struct BinPack *daten, char **text, struct ClientConfig *config, struct ProtConfig *pc){
  /* doppelte Pakete verwerfen */
  if(daten->len > 0 && 
      daten->len == pc->lastrecv.len &&
      memcmp(pc->lastrecv.daten,daten->daten,daten->len) == 0)
    return 0;
  /* paket aufheben */
  pc->lastrecv.daten = realloc(pc->lastrecv.daten, daten->len);
  if(! pc->lastrecv.daten) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
  }
  memcpy(pc->lastrecv.daten,daten->daten,daten->len);
  pc->lastrecv.len = daten->len;

  /* entschluesseln */
  struct BinPack datendecrypt;
  Cdecrypt(daten, &datendecrypt, pc->keys);

  /* auspacken */
  struct BinPack xml;
  int unpackok = unpack(&datendecrypt, &xml);
  free(datendecrypt.daten);
  if(!unpackok)
    return 0;
  


  xmlSetStructuredErrorFunc(NULL,xmlErrorHandler);
  /* auf gueltigkeit pruefen*/  
  xmlDoc *doc = xmlParseMemory(xml.daten, xml.len);
  if(! doc) {
        return 0;
    }
    xmlXPathInit();

    xmlXPathContext *ctx = xmlXPathNewContext(doc);
    if(! ctx) {
      return 0;
    }
    xmlXPathRegisterNs(ctx,(unsigned char*) CONTEXT, (unsigned char*) CONTEXTURL);

    const xmlChar *id_expr = (unsigned char*)"/lc:chat/lc:id[@content='text']/text()";
    xmlXPathObject *id_obj = xmlXPathEvalExpression(id_expr, ctx);
    if(! id_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    config->id = (char*) xmlXPathCastToString(id_obj);
    if(strlen(config->id)<=0)
      return 0;
    
    const xmlChar *name_expr = (unsigned char*)"/lc:chat/lc:name[@content='text']/text()";
    xmlXPathObject *name_obj = xmlXPathEvalExpression(name_expr, ctx);
    if(! name_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    config->username = (char*) xmlXPathCastToString(name_obj);

    /* suchen ob der client schon mal was geschickt hatte */
    int clients = 0;
    int clientda = 0;
    while((pc->clients[clients]).host != 0){
      if(strcmp(pc->clients[clients].id, config->id) == 0){
        clientda = 1;
        break;
      }
      clients++;
    }
    /* bei neuem client den speicher entsprechend erweitern */
    if(!clientda){
      pc->clients = realloc(pc->clients ,sizeof(struct ClientConfig) * (clients + 2));
      if(! pc->clients) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
      }
      memset(&(pc->clients[clients+1]),0,sizeof(struct ClientConfig));
      /* bei fremder ip diese zur templiste hinzufgen */
      struct in_addr aktuelleAddr;
      aktuelleAddr.s_addr = htonl(config->host);      
      addTempHost(&aktuelleAddr, pc);     
      
    }

    const xmlChar *roomlen_expr = (unsigned char*) "/lc:chat/lc:room/attribute::len";
    xmlXPathObject *roomlen_obj = xmlXPathEvalExpression(roomlen_expr, ctx);
    if(! roomlen_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    int roomlen = xmlXPathCastToNumber(roomlen_obj);
    
    const xmlChar *room_expr = (unsigned char*)"/lc:chat/lc:room/text()";
    xmlXPathObject *room_obj = xmlXPathEvalExpression(room_expr, ctx);
    if(! room_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    xmlChar *room = xmlXPathCastToString(room_obj);
    
    struct BinPack roombin;
    hexbin(room, &roombin);
    config->room = roombin.daten;
    config->roomlen = roombin.len;
    //fprintf(stderr, "room: %s\n", room);
    if(roomlen != roombin.len)
      return 0;
    /* testen obs auch der richtige raum fuer uns ist */
    if(memcmp(roombin.daten,pc->keys->room_md5,roombin.len) != 0)
      return 0;

    const xmlChar *sec_expr = (unsigned char*) "/lc:chat/lc:time/lc:sec/text()";
    xmlXPathObject *sec_obj = xmlXPathEvalExpression(sec_expr, ctx);
    if(! sec_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    int sec = xmlXPathCastToNumber(sec_obj);
    const xmlChar *usec_expr = (unsigned char*) "/lc:chat/lc:time/lc:usec/text()";
    xmlXPathObject *usec_obj = xmlXPathEvalExpression(usec_expr, ctx);
    if(! usec_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    int usec = xmlXPathCastToNumber(usec_obj);
    
    if(sec <= config->sec && usec <= config->usec)
      return 0;

    config->sec = sec;
    config->usec = usec;
    /* testen obs nicht ein altes paket ist */
    if(sec < pc->clients[clients].sec)
      return 0;
    if(sec == pc->clients[clients].sec && usec <= pc->clients[clients].usec)
      return 0;
      
    /* statusmeldungen lesen wenn vorhanden */
    const xmlChar *notify_expr = (unsigned char*)"/lc:chat/lc:notify[@content='text']/text()";
    xmlXPathObject *notify_obj = xmlXPathEvalExpression(notify_expr, ctx);
    if(! notify_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    char *notify = (char*)xmlXPathCastToString(notify_obj);
    if(strlen(notify) > 0){
      config->notify = notify;
      if(strcmp(notify, "online") == 0){
        if(pc->lastsend.len > 1){
          chatsend(&(pc->lastsend), pc, NULL);
        }else{
          struct BinPack sendtext;
          pc->chatConfig->notify = strdup("pong");
          packtext("", &sendtext, pc);
          chatsend(&sendtext, pc, NULL);
          free(sendtext.daten);
        }
      }
      if(strcmp(notify, "ping") == 0){
        struct BinPack sendtext;
        pc->chatConfig->notify = strdup("pong");
        packtext("", &sendtext, pc);
        chatsend(&sendtext, pc, NULL);
        free(sendtext.daten);
      }
    }else{
      config->notify = NULL;
    }

    /* die eigentliche nachricht lesen */
    const xmlChar *msg_expr = (unsigned char*)"/lc:chat/lc:msg[@content='text']/text()";
    xmlXPathObject *msg_obj = xmlXPathEvalExpression(msg_expr, ctx);
    if(! msg_obj) {
      fprintf(stderr, "xmlXPathEvalExpression call failed.\n");
      return 0;
    }
    *text = (char*) xmlXPathCastToString(msg_obj);
    /* client (chatpartner) config aufheben zum späteren vergleich */
    freeClientConfigInhalt(&(pc->clients[clients])); 
    memcpy(&(pc->clients[clients]),config,sizeof(struct ClientConfig));
    chatsend(&(pc->lastrecv), pc, config);
   
    return 1;
}

/* eine pinganfrage versenden und bei jeder clientconfig vermerken */
int Cping(struct ProtConfig *pc){
    int clients = 0;
    while((pc->clients[clients]).host != 0){
      pc->clients[clients].notify = strdup("ping");
      clients++;
      
    }
    struct BinPack sendtext;
    pc->chatConfig->notify = strdup("ping");
    packtext("", &sendtext, pc);
    chatsend(&sendtext, pc, NULL);
    free(sendtext.daten);
    return 1;
}

/* daten mit zlib paken */
int pack(struct BinPack *uncomp, struct BinPack *comp){
  uLong uncomprlen = (uLong) uncomp->len;

  uLong comprLen = 10000*sizeof(int);
  Byte *compr    = (Byte*)calloc((uInt)comprLen, 1);
  if(! compr) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
  }
  int err = compress(compr, &comprLen, (const Bytef*)uncomp->daten, uncomprlen);
  if (err != Z_OK)
    return 0;
  comp->daten = (char *) compr;
  comp->len = (int) comprLen;

  return 1;

}

/*daten mit zlib auspacken */
int unpack(struct BinPack *comp, struct BinPack *uncomp){
  uLong comprLen = (uLong) comp->len;

  uLong uncomprLen = 10000*sizeof(int);
  Byte *uncompr    = (Byte*)calloc((uInt)uncomprLen, 1);
  if(! uncompr) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
  }
  int err = uncompress(uncompr, &uncomprLen, (const Bytef*)comp->daten, comprLen);

  if (err != Z_OK)
    return 0;
  uncomp->daten = (char *) uncompr;
  uncomp->len = (int) uncomprLen;

  return 1;
}
