/* Copyright(C) 2005 Jochen Roessner <jochen@lugrot.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* die config aller sockets holen und in ifc ablegen */

#include "chatcrypt.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define KEYLEN 128

/* BinPack text nach daten verschlüsseln */ 
int Cencrypt(const struct BinPack *text, struct BinPack *daten, struct md5key *keys){
  int i, blocks = (text->len -1) / 16 + 1;
  daten->len = blocks * 16;

  /* speicher für die daten holen */
  daten->daten = calloc(blocks, 16);
  if(! daten->daten) {
    perror("lanchat");
  }
  /* zwischenspeicher für die eingabedaten holen
    und hinein kopieren */
  unsigned char *input_copy = calloc(blocks, 16);
  if(! input_copy) {
    perror("lanchat");
  }

  memcpy(input_copy, text->daten, text->len);

  /* blockweisse verschlüsseln */
  for(i = 0; i < blocks; i ++)
    AES_encrypt(input_copy + (16 * i), (unsigned char *)(daten->daten) + (16 * i), &keys->key_enc);
  

  free(input_copy);
  return 1;
}

/* BinPack daten nach text entschlüsseln */
int Cdecrypt(const struct BinPack *daten, struct BinPack *text, struct md5key *keys){

  int i, blocks = (daten->len -1) / 16 + 1;

  /* speicher für entschlüsselte daten holen */
  text->daten = calloc(blocks, 16);
  if(! text->daten) {
    perror("lanchat");
  }
  /* zwischenspeicher für daten holen 
     und daten kopieren */
  char *recvblocks_copy = calloc(blocks, 16);
  if(! recvblocks_copy) {
    perror("lanchat");
  }
  memcpy(recvblocks_copy, daten->daten, daten->len);
 
  /* blockweise entschlüsseln */
  for(i = 0; i < blocks; i ++)
    AES_decrypt((unsigned char *) recvblocks_copy + (16 * i), (unsigned char *) (text->daten) + (16 * i), &keys->key_dec);

  text->len = blocks * 16;
  free(recvblocks_copy);
  return 1;
}


/* aus dem eingabetext alle keys und md5summen erstellen */
int makemd5(const char *keytext, struct md5key *keys){
/* den key und die hashes erstellen */
  
    /*schluessel-md5 aus text erstellen */
    MD5((unsigned char *) keytext, strlen(keytext), (unsigned char *) &keys->key_md5);
    /*raum name aus text kreieren */
    char *userroom = malloc(strlen(keytext)*2+1);
    if(! userroom) {
                fprintf(stderr, "alloc schlug fehl.\n");
                return 0;
    }
    
    *userroom = 0;
    strcat(userroom, keytext);
    strcat(userroom, keytext);
    /*raum-md5 aus text erstellen */
    MD5((unsigned char *) userroom, strlen(userroom), (unsigned char *) &keys->room_md5);
    free(userroom);
    /*die schluessel erstellen*/
    if(AES_set_encrypt_key((unsigned char *) keys->key_md5, KEYLEN, &keys->key_enc)) {
      fprintf(stderr, "Fehler beim eKey erstellen.\n");
    }
    if(AES_set_decrypt_key((unsigned char *) keys->key_md5, KEYLEN, &keys->key_dec)) {
      fprintf(stderr, "Fehler beim dKey erstellen.\n");
    }
    return 1;
}
