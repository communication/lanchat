/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"


int config_read(GtkWidget *window, const char *filename, const char *profile) {
  GKeyFile* key;
  GtkWidget *input, *keylabel;
  struct ChatConfig *config;
  config=(struct ChatConfig *)lookup_widget(window, "config");
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(window, "guiconfig");
  keylabel=lookup_widget(window, "keylabel");

  key=g_key_file_new ();
  if (!g_key_file_load_from_file(key, filename,
				G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS ,
				NULL)) {
    fprintf(stderr,"Fehler beim Laden der Config-Datei\n");
    return FALSE;
  }
  if (! g_key_file_has_group(key, profile)) {
    fprintf(stderr,"Es gibt kein Profile %s", profile);
    return FALSE;
  }


  /* Username */
  if (!g_key_file_has_key(key, profile, "username", NULL)) 
    config->username=(char *)strdup(getenv("LOGNAME"));
  else {
    char *value;
    value=g_key_file_get_value(key, profile, "username", NULL);
    config->username=(char *)strdup(value);
    free(value);
  }
  /* Oggdir */
  if (!g_key_file_has_key(key, profile, "oggdir", NULL)) 
    gconfig->oggdir=NULL;
  else {
    char *value;
    value=g_key_file_get_value(key, profile, "oggdir", NULL);
    gconfig->oggdir=(char *)strdup(value);
    free(value);
  }
  /* Play Sound? */
  if (!g_key_file_has_key(key, profile, "play_sound", NULL)) 
    gconfig->play_sound=TRUE;
  else 
    if(g_key_file_get_boolean(key, profile, "play_sound", NULL)) 
      gconfig->play_sound=TRUE;
    else 
      gconfig->play_sound=FALSE;

  /* Id */
  if (!g_key_file_has_key(key, profile, "id", NULL)) {
    make_id(&config->id);
  }
  else {
    char *value;
    value=g_key_file_get_value(key, profile, "id", NULL);
    config->id=(char *)strdup(value);
    free(value);
  }
  /* Keylabel */
  if (!g_key_file_has_key(key, profile, "show_key_label", NULL)) {
    gtk_widget_hide(keylabel);
    gconfig->show_key_label=FALSE;
    }
  else {
    if(g_key_file_get_boolean(key, profile, "show_key_label", NULL)) {
      gtk_widget_show(keylabel);
      gconfig->show_key_label=TRUE;
    }
    else {
      gtk_widget_hide(keylabel);
      gconfig->show_key_label=FALSE;
    }
  }
  /* Userkey */
  if (!g_key_file_has_key(key, profile, "key", NULL)) 
    config->userkey=(char *)strdup("foo");
  else {
    char *value;
    value=g_key_file_get_value(key, profile, "key", NULL);
    config->userkey=(char *)strdup(value);
    free(value);
  }
  /* Completion */
  input=lookup_widget(window, "input");
  GtkEntryCompletion *completion;
  completion=(GtkEntryCompletion *)lookup_widget(window, "completion");
  if (!g_key_file_has_key(key, profile, "completion", NULL))
    gtk_entry_set_completion (GTK_ENTRY(input),completion);
  else {
    if (g_key_file_get_boolean(key, profile, "completion", NULL)) 
      gtk_entry_set_completion (GTK_ENTRY(input),completion);
    else 
      gtk_entry_set_completion (GTK_ENTRY(input),NULL);
  }

  config->hostlistlen=0;
  config->hostlist = malloc(sizeof(char *));
  /* Hostlist */
  if (!g_key_file_has_key(key, profile, "hosts", NULL)) 
    return TRUE;
  else {
    char **hosts;
    hosts=g_key_file_get_string_list(key, profile, "hosts", NULL, NULL);
    int i=0;
    if (hosts==NULL) {
      return TRUE;
    }
    config->hostlistlen=0;
    config->hostlist = malloc(sizeof(char *));
    while(hosts[i]) {

      struct hostent *hostdaten;
      hostdaten = gethostbyname(hosts[i]);
      if(hostdaten){
	config->hostlist=realloc(config->hostlist, 
				 sizeof(char *) * (config->hostlistlen + 1));
	config->hostlist[config->hostlistlen] = malloc(strlen(hosts[i]) + 1);
	*config->hostlist[config->hostlistlen] = '\0';
	strcat(config->hostlist[config->hostlistlen],hosts[i]);
	config->hostlistlen++;

      }
      i++;

    }
    g_strfreev (hosts);
  }
  return TRUE;
}

int config_write(GtkWidget *window, const char *filename, const char *profile) {
  FILE *conffile = fopen(filename, "r");
  GKeyFile* key;
  GtkWidget *input;
  struct ChatConfig *config;
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(window, "guiconfig");
  input=lookup_widget(window, "input");
  config=(struct ChatConfig *)lookup_widget(window, "config");

  key=g_key_file_new ();
  if (conffile == NULL 
      || !g_key_file_load_from_file(key, filename,
				 G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS ,
				 NULL)) {
      FILE *conffile = fopen(filename, "w+");
      fprintf(conffile,"[default]\nusername=%s\nkey=foo\n", getenv("LOGNAME"));
      fflush(conffile);
      fclose(conffile);
  }

  g_key_file_load_from_file(key, filename,
			    G_KEY_FILE_KEEP_COMMENTS|G_KEY_FILE_KEEP_TRANSLATIONS ,
			    NULL);

  g_key_file_set_string (key, profile, "username", config->username);
  if (gconfig->oggdir!=NULL)
    g_key_file_set_string (key, profile, "oggdir", gconfig->oggdir);

  g_key_file_set_string (key, profile, "id", config->id);
  g_key_file_set_string (key, profile, "key", config->userkey);
  g_key_file_set_string_list(key, profile, "hosts", (const char **)config->hostlist, 
			     config->hostlistlen);
  g_key_file_set_boolean(key, profile, "show_key_label", gconfig->show_key_label);
  g_key_file_set_boolean(key, profile, "play_sound", gconfig->play_sound);

  if (gtk_entry_get_completion(GTK_ENTRY(input)) != NULL) 
    g_key_file_set_boolean(key, profile, "completion", TRUE);
  else
    g_key_file_set_boolean(key, profile, "completion", FALSE);
  /* Speichern */

  char *key_file= g_key_file_to_data(key, NULL, NULL);
  conffile=fopen(filename,"w+");
  fprintf(conffile, "%s", key_file);
  fclose(conffile);  
  return TRUE;
}
