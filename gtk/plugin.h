/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 lanchat - a chat program for the lan

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#ifndef _PLUGIN
#define _PLUGIN

typedef void (* LanChatInitFunc)(char **name, char **descrip, char **author, char **version);
typedef void (* LanChatDeinitFunc)();
typedef void (* LanChatRecvCb)(struct ClientConfig *msgconfig, char *message);
typedef void (* LanChatCommand)(char *commandline);

int loadPlugin(char *filename);
void unloadPlugin(char *filename);
void lan_cb_recv_call(struct ClientConfig *msgconfig, char *message);
int command_bind(char *command, LanChatCommand cb);
void command_unbind(int id);
void command_call(char *cl);
int command_get_id(char *command);
void get_plugin_by_name(char *name, char **filename);


struct LanChatPlugin {
  GModule *module;
  char *name;
  char *filename;
  char *descrip;
  char *author;
  char *version;
  LanChatInitFunc init;
  LanChatDeinitFunc deinit;
  LanChatRecvCb recv_cb;
};

struct Command {
  char *command;
  LanChatCommand cb;
  int active;
};

#endif
