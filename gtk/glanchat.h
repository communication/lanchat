/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* The Headerfile for glanchat.
 * Here are all Functions defined 
 */

#ifndef _HAVE_INCLUDE
#define _HAVE_INCLUDE

/* Including Header-Files */
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#include <openssl/aes.h>
#include <openssl/md5.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <regex.h>
#include <dirent.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <gdk/gdk.h>
#include <pthread.h>

#include "lanlib.h"
#include "plugin.h"

#ifndef _MAIN
extern GtkWidget *chat;
#endif

/* Defining Constants */
#define BUF 1024

/* Structs */
struct GuiConfig {
  char **history;
  int historylen;
  int histact;
  char *oggdir;
  int play_sound:1;
  int show_key_label:1;
};

/* From callback.c */
void destroy(GtkWidget *window, gpointer data);
void destroy_widget(GtkWidget *window, gpointer data);
gint delete_event(GtkWidget *window, GdkEvent *event, gpointer data);
void on_send_clicked (GtkWidget *button,gpointer data);
void on_about_clicked(GtkWidget *button, gpointer data);
void on_refresh_clicked(GtkWidget *button, gpointer data);


/* From functions.c */
void send_message(struct ProtConfig *config,char *message);
void insert_message(char *author, char *message, char *color_a);
void reload_userlist();
void send_hello();
void send_quit(char *reason);
void get_color_by_name(struct ClientConfig *msgconfig, char **color);
void make_id(char **id);
void get_history_up(GtkWidget* widget, gpointer data);
void get_history_down(GtkWidget* widget, gpointer data);
void insert_string(char *message, char *color, int italic, int bold);
void get_protconfig(struct ProtConfig **config);
void playSound(char * text);

/* From interface.c */
GtkWidget *create_user_list(GtkWidget *window);
GtkWidget *create_toolbar(GtkWidget *window);
GtkWidget *create_chat_window();
void reinit_window();

/* From recieve.c */
int init_recieve();
gboolean recieve_callback( GIOChannel *source, GIOCondition condition,gpointer data);

/* From Support.c */
GtkWidget* lookup_widget(GtkWidget *widget,const gchar *widget_name);

/* From config.c */
int config_read(GtkWidget *window, const char *filename, const char *profile);
int config_write(GtkWidget *window, const char *filename, const char *profile);

/* From command.c */
void quit_cb(char *cl);
void key_cb(char *cl);
void host_cb(char *cl);
void set_cb(char *cl);
void profile_cb (char *cl);
void ping_cb(char *cl);
void rm_cb(char *cl);
void nick_cb(char *cl);

#endif

