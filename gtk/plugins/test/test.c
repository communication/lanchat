/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "../../glanchat.h"

int echo_id;

void echo(char *test);

void lanchat_init(char **name, char **descrip, char **author, char **version);

void lanchat_init(char **name, char **descrip, char **author, char **version){
  *name=strdup("test-plugin");
  *descrip=strdup("Only a Test-Plugin");
  *author=strdup("Christian Dietrich <stettberger@gmx.de>");
  *version=strdup("0.01");
  echo_id=command_bind("get", echo);
}

void echo(char *test) {
  printf("echo: %s\n", test);
}
void lanchat_deinit(){
  command_unbind(echo_id);
}
void lanchat_send_cb(char *message){
  printf("%s\n", message);
}
void lanchat_recv_cb(struct ClientConfig *msgconfig, char *message) {
  printf("<%s> %s\n", msgconfig->username, message);
}

