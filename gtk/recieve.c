/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"

int init_recieve() {
  /* Config holen */
  struct ChatConfig *config;
  struct sockaddr_in  servAddr;
  config=(struct ChatConfig *)lookup_widget(chat, "config");
  int rxsock; //empfangssocket
  rxsock = socket (AF_INET, SOCK_DGRAM, 0);
  if (rxsock < 0) {
    fprintf (stderr, "Kann RX Socket nicht �ffnen\n");
    return(1);
  }
  
  /* Lokalen Server Port bind(en) */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl (INADDR_ANY);
  servAddr.sin_port = htons (config->port);
    
  /* socket optionen setzen*/
  int i_opt = 1; //socketoption
  setsockopt(rxsock, SOL_SOCKET, SO_REUSEADDR, &i_opt, sizeof( i_opt ));
    
  int rc;
  rc = bind ( rxsock, (struct sockaddr *) &servAddr,
	      sizeof (servAddr));
  if (rc < 0) {
    fprintf (stderr, "Kann die Portnummern %d nicht bind(en)\n", config->port);
    return(1);
  }
  return rxsock;
}

gboolean recieve_callback( GIOChannel *source, GIOCondition condition,gpointer data) {
  int sock;
  //  struct sockaddr_in cliAddr;
  struct BinPack recvdaten;
  struct ClientConfig *msgconfig;
  recvdaten.daten=malloc(BUF);
  /* Config holen */
  struct ProtConfig *pc;
  pc=(struct ProtConfig *)lookup_widget(data, "proto");
  /* TextView holen */
  GtkWidget *text_view;
  text_view=lookup_widget(data, "text");
  sock = g_io_channel_unix_get_fd (source);
  msgconfig=chatrecv(&recvdaten,sock);
  char *ausgabetext;
  if(entpacktext(&recvdaten, &ausgabetext, msgconfig, pc)){
    reload_userlist(data);
    /* Funktions der Plugins aufrufen */
    lan_cb_recv_call(msgconfig, ausgabetext);
    if(msgconfig->notify != NULL &&!strcmp(msgconfig->notify, "online")){
      if (strcmp(msgconfig->username, pc->chatConfig->username)){
	char *out=malloc(strlen(msgconfig->username)+25);
	sprintf(out, "%s hat den Raum betreten\n", msgconfig->username);
	insert_string(out, "red", TRUE, TRUE);
      }
    }
    else if(msgconfig->notify != NULL && !strcmp(msgconfig->notify, "offline")){
      if (strncmp(msgconfig->username, pc->chatConfig->username, 
		  strlen(pc->chatConfig->username))){
	/* Nutzer aus der Liste entfernen */
	reload_userlist(data);
    	if ((strlen(ausgabetext))!=0) {
	  char *out=malloc(strlen(msgconfig->username)+strlen(ausgabetext)+36);
	  sprintf(out, "%s hat den Raum verlassen (Grund: %s)\n",
		  msgconfig->username, ausgabetext);
	  insert_string(out, "red", TRUE, TRUE);
	  free(out);
	}
	else {
	  char *out=malloc(strlen(msgconfig->username)+26);
	  sprintf(out, "%s hat den Raum verlassen\n", msgconfig->username);
	  insert_string(out, "red", TRUE, TRUE);
	  free(out);
	}
      }
    }
    else if(msgconfig->notify != NULL && !strcmp(msgconfig->notify, "rename")){
      char *test;
      get_color_by_name(msgconfig, &test);
      insert_message(msgconfig->username, ausgabetext, test);
    }
    else if(msgconfig->notify != NULL ){}
    else {
      char *test;
      /* Ganz normale Messages eintragen */
      get_color_by_name(msgconfig, &test);
      insert_message(msgconfig->username, ausgabetext, test);
      /* Sound abspielen */
      pthread_t thread;
      char *ausgabetextfork = strdup(ausgabetext);
      pthread_create(&thread, NULL, (void *)playSound, ausgabetextfork);
    }
    free(ausgabetext);
    free(msgconfig);
  }
  free(recvdaten.daten);

  return TRUE;
}

