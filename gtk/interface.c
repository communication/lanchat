/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"

GtkWidget *create_user_list(GtkWidget *window) {
  GtkWidget *scrolled, *list_view;
  GtkListStore *store;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *column;
  /* Scrolled */
  scrolled=gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled), GTK_SHADOW_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), 
				  GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
  /* List */
  store = gtk_list_store_new (1,G_TYPE_STRING);
  list_view=gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
  renderer = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new_with_attributes ("Nutzer",
						     renderer,
						     "text",
						     0,
						     NULL);
  gtk_tree_view_column_set_sort_column_id (column, 0);
  gtk_tree_view_append_column (GTK_TREE_VIEW(list_view), column);
  gtk_object_set_data (GTK_OBJECT (window), "user_list", list_view);
  gtk_widget_set_size_request            (GTK_WIDGET(scrolled), 150,-1);
  gtk_container_add (GTK_CONTAINER (scrolled), list_view);			 

  return scrolled;
}

GtkWidget *create_toolbar(GtkWidget *window) {
  GtkWidget *table, *quit, *about, *refresh;
  GtkWidget *quit_image, *about_image, *refresh_image;
  GtkTooltips *tool_tips;
   
  tool_tips = gtk_tooltips_new ();
  table=gtk_table_new(1,3/*COLS*/, FALSE);


  /* Refresh */
  refresh=gtk_button_new();
  refresh_image=gtk_image_new_from_stock(GTK_STOCK_REFRESH, GTK_ICON_SIZE_LARGE_TOOLBAR);
  g_signal_connect(G_OBJECT(refresh), "clicked", G_CALLBACK(on_refresh_clicked), window);
  gtk_container_add(GTK_CONTAINER(refresh), refresh_image);
  gtk_object_set_data(GTK_OBJECT(window), "refresh", refresh);
  gtk_tooltips_set_tip (GTK_TOOLTIPS (tool_tips), refresh,
			"Aktualisiert die Liste der anwesenden Nutzer",
			NULL);
  gtk_table_attach(GTK_TABLE(table), refresh, 0,1,0,1,GTK_FILL,GTK_FILL, 0,0);

  /* About */
  about=gtk_button_new();
  about_image=gtk_image_new_from_stock(GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_LARGE_TOOLBAR);
  g_signal_connect(G_OBJECT(about), "clicked", G_CALLBACK(on_about_clicked), NULL);
  gtk_container_add(GTK_CONTAINER(about), about_image);
  gtk_tooltips_set_tip (GTK_TOOLTIPS (tool_tips), about,
			"Informationen zu GLanChat",
			NULL);
  gtk_table_attach(GTK_TABLE(table), about, 1,2,0,1,GTK_FILL,GTK_FILL, 0,0);

  /* Quit */
  quit=gtk_button_new();
  quit_image=gtk_image_new_from_stock(GTK_STOCK_QUIT, GTK_ICON_SIZE_LARGE_TOOLBAR);
  g_signal_connect(G_OBJECT(quit), "clicked", G_CALLBACK(destroy), NULL);
  gtk_container_add(GTK_CONTAINER(quit), quit_image);
  gtk_tooltips_set_tip (GTK_TOOLTIPS (tool_tips), quit,
			"Beendet GLanChat",
			NULL);
  gtk_table_attach(GTK_TABLE(table), quit, 2,3,0,1,GTK_FILL,GTK_FILL, 0,0);

  
  return table;

}

/* Creates the Chat - Window */
GtkWidget *create_chat_window() {
  GtkWidget *window, *send, *text,*scrolled, *table, *input, *toolbar, *user_list, *keylabel, 
    *dummy;
  GtkWidget *refresh, *tmp;
  GtkTextBuffer *buffer;
  GtkAccelGroup *accel_group, *input_line;
  GtkListStore *store;
  GtkEntryCompletion *completion;
  
  accel_group = gtk_accel_group_new ();
  input_line =gtk_accel_group_new ();
  window=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window),"GLanChat");
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 600);
  g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy), window); 

  table=gtk_table_new(5,4, FALSE);
  gtk_table_set_row_spacings (GTK_TABLE (table), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table), 5);

  gtk_container_add(GTK_CONTAINER(window), table);

  /* Toolbar */
  toolbar=create_toolbar(window);
  gtk_table_attach(GTK_TABLE(table), toolbar, 0,4,0,1,GTK_FILL,GTK_FILL, 0,0);
  refresh=lookup_widget(window, "refresh");
  gtk_widget_add_accelerator (refresh, "clicked", accel_group,
			      GDK_F5, 0,
                              GTK_ACCEL_VISIBLE);

  /* Key-Label */
  keylabel=gtk_label_new("");
  gtk_label_set_markup (GTK_LABEL(keylabel), 
			"<span foreground=\"black\" size=\"large\">Raum/Key:</span>");
  gtk_object_set_data (GTK_OBJECT (window), "keylabel", keylabel);
  gtk_table_attach(GTK_TABLE(table), keylabel, 0,4,1,2,GTK_FILL,GTK_FILL, 0,0);
  
  /* User-List */
  user_list=create_user_list(window);
  gtk_table_attach(GTK_TABLE(table), user_list, 3,4,2,4,GTK_FILL,GTK_FILL, 0,0);
  

  /* Text-View  with Scrolled*/
  text=gtk_text_view_new();
  gtk_object_set_data (GTK_OBJECT (window), "text", text);

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text));
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(text),  GTK_WRAP_WORD_CHAR);
  gtk_text_view_set_editable(GTK_TEXT_VIEW(text),FALSE);
  gtk_text_view_set_indent (GTK_TEXT_VIEW(text), -30);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(text),FALSE);
  scrolled=gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled), GTK_SHADOW_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), 
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  GTK_WIDGET_UNSET_FLAGS (text, GTK_CAN_FOCUS);
  gtk_container_add (GTK_CONTAINER (scrolled), text);
  gtk_table_attach_defaults(GTK_TABLE(table), scrolled, 0,3,2,4);

  /* input */
  input=gtk_entry_new();
  gtk_object_set_data (GTK_OBJECT (window), "input", input);
  gtk_widget_add_accelerator (refresh, "clicked", accel_group,
			      GDK_F5, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_table_attach(GTK_TABLE(table), input, 0,3,4,5,GTK_FILL,GTK_FILL, 0,0);
  /* Dummy */
  dummy=gtk_button_new();
  g_signal_connect(GTK_OBJECT(dummy), "clicked", G_CALLBACK(get_history_up), window);
  gtk_widget_add_accelerator (dummy, "clicked", accel_group,
			      GDK_Page_Up, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_widget_add_accelerator (dummy, "clicked", accel_group,
			      GDK_KP_Page_Up, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_table_attach(GTK_TABLE(table), dummy, 1,2,2,3,GTK_FILL,GTK_FILL, 0,0);
  dummy=gtk_button_new();
  g_signal_connect(GTK_OBJECT(dummy), "clicked", G_CALLBACK(get_history_down), window);
  gtk_widget_add_accelerator (dummy, "clicked", accel_group,
			      GDK_Page_Down, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_widget_add_accelerator (dummy, "clicked", accel_group,
			      GDK_KP_Page_Down, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_table_attach(GTK_TABLE(table), dummy, 1,2,2,3,GTK_FILL,GTK_FILL, 0,0);
  /* Entry Completion */
  completion=gtk_entry_completion_new();
  tmp=(GtkWidget *)lookup_widget(window, "user_list");
  store=GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(tmp)));
  gtk_entry_completion_set_model  (completion,GTK_TREE_MODEL(store));
  gtk_entry_completion_set_minimum_key_length(completion, 1);
  gtk_entry_completion_set_text_column (completion, 0);
  gtk_entry_set_completion (GTK_ENTRY(input), completion);
  gtk_object_set_data (GTK_OBJECT (window), "completion", completion);
  
  /* send */
  send=gtk_button_new_with_label("Senden");
  g_signal_connect(G_OBJECT(send), "clicked", G_CALLBACK(on_send_clicked), window);
  gtk_widget_add_accelerator (send, "clicked", accel_group,
                              GDK_Return, 0,
                              GTK_ACCEL_VISIBLE);
  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);
  gtk_table_attach(GTK_TABLE(table), send, 3,4,4,5,GTK_FILL,GTK_FILL, 0,0);


  g_signal_connect (G_OBJECT (window), "delete_event", G_CALLBACK (delete_event), text);
  gtk_widget_grab_focus(input);
  gtk_widget_show_all(window);
  return window;
}

void reinit_window() {
  GtkWidget *keylabel;
  struct ChatConfig *config;
  char *markup;
  
  config=(struct ChatConfig *) lookup_widget(chat, "config");
  keylabel=lookup_widget(chat, "keylabel");


  markup=g_markup_printf_escaped("<span foreground=\"black\" size=\"large\">Raum/Key: %s</span>",
				    config->userkey);
  gtk_label_set_markup (GTK_LABEL (keylabel), markup);
  g_free (markup);
}
