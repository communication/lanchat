/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"

/* When /quit was called */

void quit_cb(char *cl) {
  /* bin weg senden */
  char *reason=strchr(cl, ' ');
  send_quit(reason);
  destroy(NULL,chat);
}

void key_cb(char *cl) {
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  char *new=strchr(cl, ' ');
  if (new!=NULL && strcmp(new, config->chatConfig->userkey)) {
    new=new+1;
    send_quit("");
    config->chatConfig->userkey = (char *)strdup(new);
    send_hello();
  }
}

void host_cb(char *cl) {
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  char *new=strchr(cl, ' ');
  struct hostent *hostdaten;
  if(new!=NULL) {
    new=new+1;
    hostdaten = gethostbyname(new);
    if(hostdaten){
      char out[strlen(new)+50];
      sprintf(out, "Host gefunden: %s\n", new);
      insert_string(out, "red", TRUE, TRUE);
      config->chatConfig->hostlist=realloc(config->chatConfig->hostlist,
					   sizeof(char *) * 
					   (config->chatConfig->hostlistlen + 1));
      config->chatConfig->hostlist[config->chatConfig->hostlistlen]=malloc(strlen(new)+1);
      *config->chatConfig->hostlist[config->chatConfig->hostlistlen] = '\0';
      strcat(config->chatConfig->hostlist[config->chatConfig->hostlistlen],new);
      config->chatConfig->hostlistlen++;
      
      struct in_addr aktuelleAddr;
      aktuelleAddr = *(struct in_addr*) hostdaten->h_addr;
      addTempHost(&aktuelleAddr, config);
    }
  }
}
 
void set_cb(char *cl) {
  char **args;
  int argc=0;

  GtkWidget *input, *text_view, *keylabel;
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(chat, "guiconfig");
  input=lookup_widget(chat, "input");
  keylabel=lookup_widget(chat, "keylabel");
  text_view=lookup_widget(chat, "text");

  g_shell_parse_argv(cl, &argc, &args, NULL);
  if (argc!=3) {
    insert_string("Falsche Anzahl an Argumenten\n", "red", TRUE, TRUE);
    gtk_entry_set_text(GTK_ENTRY(input),"");
    return;
  }
  if (!strcmp(args[1], "completion")) {
    GtkEntryCompletion *completion;
    completion=(GtkEntryCompletion *)lookup_widget(chat, "completion");
    if (!strcmp(args[2], "on")){
      gtk_entry_set_completion (GTK_ENTRY(input),completion);
      insert_string("Completion is ON\n", "red", TRUE, TRUE);
    }
    else if (!strcmp(args[2], "off")){
      gtk_entry_set_completion (GTK_ENTRY(input),NULL);
      insert_string("completion is OFF\n", "red",TRUE, TRUE);
    }
    else {
      insert_string("completion [on|off] - toggles autocompletion\n", "red",TRUE, TRUE);
    }
  }
  else if (!strcmp(args[1], "show_key_label")) {
    if (!strcmp(args[2], "on")){
      gconfig->show_key_label=TRUE;
      insert_string("show_key_label is ON\n", "red", TRUE, TRUE);
      gtk_widget_show(keylabel);
    }
    else if (!strcmp(args[2], "off")){
      gconfig->show_key_label=FALSE;
      insert_string("show_key_label is OFF\n", "red", TRUE, TRUE);
      gtk_widget_hide(keylabel);
    }
    else {
      insert_string("show_key_label [on|off] - toggles key_label\n", "red",TRUE, TRUE);
    }
        
  }
  else if (!strcmp(args[1], "play_sound")) {
    if (!strcmp(args[2], "on")){
      gconfig->play_sound=TRUE;
      insert_string("play_sound is ON\n", "red", TRUE, TRUE);
    }
    else if (!strcmp(args[2], "off")){
            gconfig->play_sound=FALSE;
            insert_string("play_sound is OFF\n", "red", TRUE, TRUE);
    }
    else {
      insert_string("play_sound [on|off] - should the sounds be played\n", "red",TRUE, TRUE);
    }
  }
  else if (!strcmp(args[1], "oggdir")) {
    if (strlen(args[2])!=0) {
      gconfig->oggdir=strdup(args[2]);
      char out[strlen(args[2])+50];
      sprintf(out, "oggdir=%s\n", args[2]);
      insert_string(out, "red",TRUE, TRUE);
    }
  }
}

void rm_cb(char *cl){
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  while(config->chatConfig->hostlistlen > 0){
    free(config->chatConfig->hostlist[config->chatConfig->hostlistlen]);
    config->chatConfig->hostlistlen--;
  } 
  insert_string("Hostliste geleert\n", "red",TRUE, TRUE);
}

void ping_cb(char *cl) {
  on_refresh_clicked(NULL, chat);
  insert_string("Ping gesendet\n", "red", TRUE, TRUE);
}

void profile_cb (char *cl){
  char **argv;
  int argc=0;
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  if(!g_shell_parse_argv(cl, &argc, &argv, NULL) || argc < 2)
    return;
  if (!strcmp(argv[1], "load")) {
    if (argc == 2){
      config_read(chat, config->chatConfig->configfilename, "default");
      insert_string("Einstellungen geladen\n", "red",TRUE, TRUE);
      reloadHostlist(config);
      send_hello();
    }
    else{
      if (config_read(chat, config->chatConfig->configfilename, argv[2])){
	insert_string("Einstellungen geladen\n", "red",TRUE, TRUE);
	reloadHostlist(config);
	send_hello();
      }
      else {
	insert_string("Kein solches Profil vorhanden\n", "red",TRUE, TRUE);
      } 
    }   
    reinit_window();
  }
  else if(!strcmp(argv[1], "save")) {
    if (argc==2){
      config_write(chat, config->chatConfig->configfilename, "default");
      insert_string("Einstellungen gespeichert\n", "red",TRUE, TRUE);
    }
    
    else {
      config_write(chat, config->chatConfig->configfilename, argv[2]);
      insert_string("Einstellungen gespeichert\n", "red",TRUE, TRUE);
    }
  }
  else {
    insert_string("/profile [load|save] [profilename]\n", "red", TRUE, TRUE);
  }
}

void nick_cb(char *cl) {
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  char *text=strchr(cl, ' ');
  if (text!=NULL) {
    text=text+1;
    config->chatConfig->notify=(char *)strdup("rename");
    char *msg=malloc(strlen(text)+50);
    sprintf(msg, "Namenswechsel von: %s", config->chatConfig->username);
    free(config->chatConfig->username);
    config->chatConfig->username=strdup(text);
    send_message(config, msg);
    on_refresh_clicked(NULL, chat);
  }
  else {
    insert_string("/nick [new nickname]\n", "red", TRUE, TRUE);
  }
}





    
