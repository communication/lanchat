/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"


void send_hello(){
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  GtkWidget *text_view;
  text_view=lookup_widget(chat, "text");
  char out[strlen(config->chatConfig->userkey)+50];
  sprintf(out, "Habe den Raum betreten\n");
  makemd5(config->chatConfig->userkey, config->keys);
  /* bin da senden */
  config->chatConfig->notify=(char *)strdup("online");
  send_message(config, "Bin auf Empfang");
  reinit_window();
  insert_string(out, "red", TRUE, TRUE);
}

void send_quit(char *reason) {
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");
  config->chatConfig->notify=(char *)strdup("offline");
  send_message(config, reason);
}

/* Hilfsfunktion */
int scroll_to_end(gpointer data) {
  GtkWidget *text_view;
  text_view=lookup_widget(chat, "text");
  GtkTextIter iter;
  GtkTextBuffer *buffer;
  buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
  gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buffer), &iter);
  gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW(text_view), &iter, 0, TRUE, 0.0, 1.0);
  return FALSE;
}

void send_message(struct ProtConfig *config,char *message){
  /* sendesequenz zusammenbauen, hash+name+: +text */
  struct BinPack sendtext;
  packtext(message, &sendtext, config);
  /* auf die sockets senden */
  chatsend(&sendtext, config, NULL);
  /* speicher freigeben */
  free(sendtext.daten);
}

void insert_message(char *author, char *message, char *color_a) {
  gdk_threads_enter();
  /* Insert Text */
  char text[strlen(author)+3];
  char mesg[strlen(message)+2];
  sprintf(text, "<%s> ", author);
  sprintf(mesg, "%s\n", message);
  insert_string(text, color_a, FALSE, TRUE);
  insert_string(mesg, "black", FALSE, FALSE);
  g_idle_add ( (GSourceFunc) scroll_to_end, NULL);
  gdk_threads_leave();
}


void reload_userlist(GtkWidget *main) {
  GtkWidget *list_view;
  GtkTreeModel *store;
  struct ProtConfig *config;
  GtkTreeIter iter;
  list_view=(GtkWidget *)lookup_widget(main, "user_list");
  store=GTK_TREE_MODEL(gtk_tree_view_get_model(GTK_TREE_VIEW(list_view)));
  config=(struct ProtConfig *)lookup_widget(main, "proto");
  int client=0;
  gtk_list_store_clear(GTK_LIST_STORE(store));
  while(config->clients[client].username!=0) {
    gtk_list_store_append (GTK_LIST_STORE(store), &iter);
    gtk_list_store_set (GTK_LIST_STORE(store), &iter,0, 
			config->clients[client].username, -1);      
    client++;
  }
}

void get_color_by_name(struct ClientConfig *msgconfig, char **color){

  struct ProtConfig *pc;
  pc=(struct ProtConfig *)lookup_widget(chat, "proto");
  int len=10;
  char *farben[] = {"#228B22", /* Green*/
		    "#0000FF", /* Blue */
		    "#FFD700", /* Yellow */
		    "#00FFFF", /* Aqua */
		    "#FF00FF", /* Violett */
		    "#FFA500", /* Orange */
		    "#B22222", /* Firebrick */
		    "#4B0082", /* Indigo */
		    "#F0E68C", /* Kahki */
		    "#FF0000", /* Red */
		    NULL};
  int clients = 0;
  while((pc->clients[clients]).username != 0){
    if (pc->clients[clients].host ==  msgconfig->host
	&& strcmp(pc->clients[clients].username, msgconfig->username) == 0){
      break;
    }
    clients++;
  }
  
  if (clients >= len) {
    *color=(char *)strdup("#FFFFFF");
    /* Vieleicht noch ein Zufallsgenerator */
  }
  else {
    *color=(char *)strdup(farben[clients]);
  }
}

void make_id(char **id) {
  char random[65];
  srand((unsigned) time(NULL));
  int i=0;
  while(i < 64) {
    char c=(char)rand();
    if ((c >= 'A' && c <= 'Z')
	||(c >= 'a' && c <= 'z')){
      random[i]=c;
      i++;
    }
  }
  random[64]=0;
  *id=strdup(random);
}

void get_history_up(GtkWidget* widget, gpointer data){
  GtkWidget *input;
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(data, "proto");
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(data, "guiconfig");
  input=lookup_widget(data, "input");
  if (gconfig->histact < 1 || gconfig->histact > gconfig->historylen){
    return;
  }
  else {
    gtk_entry_set_text(GTK_ENTRY(input), gconfig->history[gconfig->historylen-gconfig->histact]);
    gconfig->histact++;
  }
}



void get_history_down(GtkWidget* widget, gpointer data){
  GtkWidget *input;
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(data, "proto");
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(data, "guiconfig");
  input=lookup_widget(data, "input");  
  if (gconfig->histact < 1 || gconfig->histact-2 > gconfig->historylen){
    return;
  }
  else {
    gconfig->histact--;
    if ((gconfig->histact-1)==1 || gconfig->histact==0){
      gtk_entry_set_text(GTK_ENTRY(input), "");
      gconfig->histact=1;
    }
    else{
      gtk_entry_set_text(GTK_ENTRY(input), 
			 gconfig->history[gconfig->historylen-(gconfig->histact-1)]);
    }

  }
}

void get_protconfig(struct ProtConfig **config){
  *config=(struct ProtConfig *)lookup_widget(chat, "proto");
}

void insert_string(char *message, char *color, int italic, int bold){
  GtkTextIter end;
  GtkTextTag *color_tag, *bold_tag, *italic_tag;
  GtkTextBuffer* buffer;
  GtkWidget *text_view;
  text_view=lookup_widget(chat, "text");
  buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_view));
  color_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER(buffer), NULL,
	   		            "foreground", color,NULL);  
  italic_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER(buffer), NULL,
	   		            "style",PANGO_STYLE_ITALIC,NULL);  
  bold_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER(buffer), NULL,
	   		            "weight",PANGO_WEIGHT_BOLD,NULL);  
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(buffer), &end);

  if (italic && bold) 
    gtk_text_buffer_insert_with_tags(buffer, &end, message, -1,color_tag, bold_tag, 
				     italic_tag, NULL);
  else if (italic)
    gtk_text_buffer_insert_with_tags(buffer, &end, message, -1,color_tag, italic_tag, NULL);
  else if (bold)
    gtk_text_buffer_insert_with_tags(buffer, &end, message, -1,color_tag, bold_tag, NULL);
  else
    gtk_text_buffer_insert_with_tags(buffer, &end, message, -1,color_tag, NULL);
}

void playSound(char *text) {
  struct GuiConfig *gui;
  gui=(struct GuiConfig *)lookup_widget(chat, "guiconfig");
  if (gui->play_sound && gui->oggdir!=NULL)
    oggerer(gui->oggdir, text);
  pthread_exit(NULL);
}

  
