/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#define _MAIN
#include "glanchat.h"

GtkWidget *chat;

int main(int argc, char *argv[]) {
  GIOChannel *sock_io;
  struct ChatConfig *chatconfig;
  struct ProtConfig *protconfig;
  struct GuiConfig guiconfig;
  struct md5key keys;
  GSList *plugins, *commands;
  char *filename;
  gtk_init(&argc, &argv);

  playinit();
  plugins=g_slist_alloc();
  commands=g_slist_alloc();

  /* Default Config */
  filename=malloc(strlen((char *)getenv("HOME"))+12);
  strcat(filename, (char *)getenv("HOME"));
  strcat(filename, "/.glanchat");

  chat=create_chat_window();

  guiconfig.history=malloc(sizeof(char *));
  guiconfig.historylen=0;
  guiconfig.histact=1;

  chatconfig = initChatConfig("");
  protconfig=protokollInit(chatconfig,&keys);
  
   /* Daten setzten */
  gtk_object_set_data (GTK_OBJECT (chat), "config", chatconfig);
  gtk_object_set_data (GTK_OBJECT (chat), "proto", protconfig);
  gtk_object_set_data (GTK_OBJECT (chat), "guiconfig", &guiconfig);
  gtk_object_set_data (GTK_OBJECT (chat), "plugins", plugins);
  gtk_object_set_data (GTK_OBJECT (chat), "commands", commands);

  /* Einstellungen */
  chatconfig->configfilename=filename;
  if (!config_read(chat, filename, "default")) {
    chatconfig->hostlist=malloc(sizeof(char *));
    chatconfig->hostlistlen=0;
    chatconfig->username=(char *)strdup(getenv("LOGNAME"));
    chatconfig->userkey=(char *)strdup("foo");
    make_id(&chatconfig->id);
  }
  reloadHostlist(protconfig);

  /* Socket + IO_Watch vorbereiten */
  int rxsock=init_recieve(chat);
  if (rxsock==1) {
    fprintf(stderr, "Fehler bei initalisieren des Socket's\n");
    return 1;
  }
  sock_io=g_io_channel_unix_new (rxsock);
  g_io_add_watch (sock_io,G_IO_IN, recieve_callback, chat);

  /* Kommandos binden */
  command_bind("quit", quit_cb);
  command_bind("q", quit_cb);
  command_bind("key", key_cb);
  command_bind("host", host_cb);
  command_bind("set", set_cb);
  command_bind("ping", ping_cb);
  command_bind("rm", rm_cb);
  command_bind("profile", profile_cb);
  command_bind("nick", nick_cb);

  send_hello();
  gtk_main();
  playshutdown();
  return 0;  
}
