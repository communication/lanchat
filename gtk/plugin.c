/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

/* The Plugin-Interface */

#include "glanchat.h"

int loadPlugin(char *filename) {
  GSList *plugins;
  struct LanChatPlugin *new;
  char *out;
  plugins = (GSList *)lookup_widget(chat, "plugins");

  /* Testen ob Plugin schon geladen wurde */
  int i;
  struct LanChatPlugin *tmp;
  for (i=1; i<(g_slist_length(plugins)); i++) {
    tmp=g_slist_nth_data(plugins, i);
    if (!strcmp(filename, tmp->filename)){
      insert_string("Plugin wurde schon einmal geladen\n", "red", TRUE, TRUE);
      return FALSE;
    }
  }
  
  /* New Plugin */
  new=malloc(sizeof (struct LanChatPlugin *)+50);
  new->module=g_module_open(filename, 0);
  if (!new->module) {
    insert_string("Fehler beim Laden des Modules\n", "red", TRUE, TRUE);
    return FALSE;
  }
  if (!g_module_symbol (new->module, "lanchat_init", (gpointer *)&new->init)){
    insert_string("Es gibt keine Init-Funktion, sicher dass es ein LanChat-Plugin ist\n",
		  "red", TRUE, TRUE);
    return FALSE;
  }
  if (!g_module_symbol (new->module, "lanchat_deinit", (gpointer *)&new->deinit))
    new->deinit=NULL;
  if (!g_module_symbol (new->module, "lanchat_recv_cb", (gpointer *)&new->recv_cb))
    new->recv_cb=NULL;
  new->filename=(char *) strdup(filename);
  new->init(&new->name, &new->descrip, &new->author, &new->version);
  plugins=g_slist_append(plugins, new);

  /* Erfolgsmeldung ausgeben */
  out=malloc(strlen(new->name)+22);
  *out=0;
  strcat(out, new->name);
  strcat(out, " erfolgreich geladen\n");
  insert_string(out, "red", TRUE, TRUE);
  free(out);
  return TRUE;
}
void unloadPlugin(char *filename){
  GSList *plugins;  
  plugins = (GSList *)lookup_widget(chat, "plugins");
  /* Nach Plugin suchen und deinit aufrufen */
  int i;
  struct LanChatPlugin *tmp;
  for (i=1; i<(g_slist_length(plugins)); i++) {
    tmp=g_slist_nth_data(plugins, i);
    if (!strcmp(filename, tmp->filename)){
      /* Plugin entfernen und Erfolg melden */
      char *out;
      out=malloc(strlen(tmp->name)+40);
      sprintf(out, "%s erfolgreich entfernt\n", tmp->name);
      if (tmp->deinit!=NULL)
	tmp->deinit();
      plugins=g_slist_remove(plugins, tmp);
      g_module_close(tmp->module);
      insert_string(out, "red", TRUE, TRUE);
      free(out);
      return;
    }
  }
  insert_string("Plugin wurde nie geladen\n", "red", TRUE, TRUE);
}

void lan_cb_recv_call(struct ClientConfig *msgconfig, char *message){
  GSList *plugins;  
  plugins = (GSList *)lookup_widget(chat, "plugins");
  int i;
  struct LanChatPlugin *tmp;
  for (i=1; i<(g_slist_length(plugins)); i++) {
    tmp=g_slist_nth_data(plugins, i);
    if (tmp->recv_cb!=NULL)
      tmp->recv_cb(msgconfig, message);
  }
}

int command_bind(char *command, LanChatCommand cb) {
  struct Command *tmp;
  GSList *commands;
  int pos;
  commands = (GSList *)lookup_widget(chat, "commands");

  int i;
  struct Command *akt;
  for (i=1; i<(g_slist_length(commands)); i++) {
    akt=g_slist_nth_data(commands, i);
      if (!strcmp(command, akt->command) && akt->active) {
	printf("%s wurde bereits gebunden, sry\n", command);
	return -1;
      }
  }
  
  tmp=malloc(sizeof(struct Command));
  tmp->command=malloc(strlen(command)+1);
  *tmp->command=0;
  strcat(tmp->command, command);
  tmp->cb=cb;
  tmp->active=TRUE;

  pos=g_slist_length(commands);
  commands = g_slist_append(commands, tmp);
  return pos;
}

void command_unbind(int id){
  struct Command *tmp;
  GSList *commands;
  commands = (GSList *)lookup_widget(chat, "commands");
  tmp=g_slist_nth_data(commands, id);
  tmp->active=FALSE;
}

void command_call(char *cl) {
  if (strlen(cl)==0)
    return;
  /* cl is without '/' fist */
  int argc;
  char **argv;
  if(!  g_shell_parse_argv(cl,&argc, &argv, NULL) && argc <1)
    return;

  GSList *commands;
  commands = (GSList *)lookup_widget(chat, "commands");

  int i;
  int called=FALSE;
  struct Command *akt;
  for (i=1; i<(g_slist_length(commands)); i++) {
    akt=g_slist_nth_data(commands, i);  
    if (!strcmp(akt->command, argv[0]) && akt->active) {
      /* function aufrufen*/
      akt->cb(cl);
      called=TRUE;
    }
  }
  if(!called) {
    insert_string("Kommando nicht gefunden\n", "red", TRUE, TRUE);
  }
}

int command_get_id(char *command) {
  GSList *commands;
  commands = (GSList *)lookup_widget(chat, "commands");

  int i;
  struct Command *akt;
  for (i=1; i< g_slist_length(commands); i++) {
    akt=g_slist_nth_data(commands, i);  
    if (!strcmp(akt->command, command) && akt->active)
      return i;
  }
    return -1;
}

void get_plugin_by_name(char *name, char **filename) {
  DIR *dp;
  struct dirent *ep;
  char *filepfad;
  char *dirname=malloc(strlen(getenv("HOME"))+50);
  sprintf(dirname, "%s/.lanchat/plugins/", getenv("HOME"));
  dp = opendir (dirname);
  if (dp != NULL){
    while ((ep = readdir (dp))){
      if(ep->d_name[0] != '.'){
        /* regex */
        char pattern[]="^.*\\.so$";
        size_t regex_nmatch=2;
        regex_t regex;
        regmatch_t regex_pmatch[regex_nmatch];
        char *match=strdup(ep->d_name);
        if (regcomp(&regex,pattern,REG_EXTENDED) == 0 
	    && regexec(&regex,match,regex_nmatch,regex_pmatch,0) == 0){
          filepfad = calloc(strlen(dirname) + strlen(ep->d_name)+1,1);
	  sprintf(filepfad, "%s%s", dirname,ep->d_name);
          if(!filepfad){
	    *filename=NULL;
            return;
	  }
	  else 
	    *filename=filepfad;
	  return;
	}
      }
    }
    closedir(dp);
  }

  
}
