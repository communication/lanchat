/* Copyright(C) 2005 Christian Dietrich <stettberger@gmx.de>
 glanchat - a frontend for lanchat by Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#include "glanchat.h"

void destroy(GtkWidget *window, gpointer data) {
  send_quit("habe Quit gemacht");
  gtk_main_quit();
}

void destroy_widget(GtkWidget *window, gpointer data){
  gtk_widget_destroy (GTK_WIDGET(data));
}
gint delete_event(GtkWidget *window, GdkEvent *event, gpointer data) {
  return FALSE;
}

void on_send_clicked (GtkWidget *button,gpointer data){
  char *text;
  GtkWidget *input, *text_view, *keylabel;
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(data, "proto");
  struct GuiConfig *gconfig;
  gconfig=(struct GuiConfig *)lookup_widget(data, "guiconfig");
  input=lookup_widget(data, "input");
  keylabel=lookup_widget(data, "keylabel");
  text_view=lookup_widget(data, "text");
  text=(char *)gtk_entry_get_text(GTK_ENTRY(input));
  if (strlen(text)!=0) {
    /* Add to History */
    gconfig->history=realloc(gconfig->history,sizeof(char *) * (gconfig->historylen + 1));
    gconfig->history[gconfig->historylen]=malloc(strlen(text)+1);
    *gconfig->history[gconfig->historylen]= '\0';
    strcat(gconfig->history[gconfig->historylen],text);
    gconfig->historylen++;
    gconfig->histact=1;

    if(text[0] == '/'){
      char *command=(char *) strdup(text+1);
      command_call(command);
      free(command);
    }
    else {
    /* Senden */
      config->chatConfig->notify=NULL;
      send_message(config,text);
    }
  }
  gtk_entry_set_text(GTK_ENTRY(input),"");

}

void on_about_clicked(GtkWidget *button, gpointer data) {
  GtkWidget *about, *logo;
  GdkPixbuf *logo_pix;
  const char *authors[]={"Jochen Roessner <j.roessner@cb-funkshop.de>", 
		  "Stefan Siegel <stesie@brokenpipe.de>", 
		   "Christian Dietrich <stettberger@gmx.de>",
		   NULL};
  about=gtk_about_dialog_new ();
  gtk_about_dialog_set_name (GTK_ABOUT_DIALOG(about), "GLanChat");
  gtk_about_dialog_set_version (GTK_ABOUT_DIALOG(about), "0.2");
  gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG(about), authors);
  logo=gtk_image_new_from_file("pics/logo.png");
  logo_pix=gtk_image_get_pixbuf(GTK_IMAGE(logo));
  gtk_about_dialog_set_logo (GTK_ABOUT_DIALOG(about), GDK_PIXBUF(logo_pix));
  gtk_window_set_position (GTK_WINDOW(about), GTK_WIN_POS_CENTER);
  gtk_widget_show(about);

}

void on_refresh_clicked(GtkWidget *button, gpointer data) {
  /* Refreshed die User-List */
  GtkWidget *list_view;
  GtkListStore *store;
  list_view=(GtkWidget *)lookup_widget(chat, "user_list");
  store=GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(list_view)));
  gtk_list_store_clear(store);
  struct ProtConfig *config;
  config=(struct ProtConfig *)lookup_widget(chat, "proto");  
  Cping(config);
}


