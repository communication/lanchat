#! /usr/bin/perl -w
use strict;

my $port = 8765;

use Socket;
socket(SOCKET, PF_INET, SOCK_DGRAM, getprotobyname("udp"))
    or die "cannot create socket: $!";
bind(SOCKET, sockaddr_in($port, INADDR_ANY));

my %connections;

for(;;) {
    #-- receive some data first ...
    my $msg;
    my $portaddr = recv(SOCKET, $msg, 16384, 0)
	or die "unable to receive datagrams: $!";
    my ($portno, $ipaddr) = sockaddr_in($portaddr);
    my $host = gethostbyaddr($ipaddr, AF_INET);

    print STDERR "got datagram from $host.\n";

    #-- update connections cache
    $connections{$ipaddr} = time();

    #-- forward to all hosts but the sender ...
    foreach(keys(%connections)) {
	next if($ipaddr eq $_); #-- ignore sender

	my $hostname = gethostbyaddr($_, AF_INET);

	if(time() - $connections{$_} > 15 * 60) {
	    # the client didn't say anything for 15 minutes,
	    # silently remove him from the list of connections
	    print STDERR "$hostname removed from clients list.\n";
	    delete $connections{$_};
	    next;
	}

	print STDERR "now passing packet to client $hostname ...\n";

	my $client = sockaddr_in($port, $_);
	unless(send(SOCKET, $msg, 0, $client) == length($msg)) {
	    warn "could send to client $hostname: $!\n";
	    delete $connections{$_}; #-- remove from cache
	}
    }
}
